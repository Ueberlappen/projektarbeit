﻿using UnityEngine;
using System.Collections;

public enum EGameMode {

	GameModeOffline = 0,
    GameModeWifi = 1,
    GameModeBluetooth = 2
}
