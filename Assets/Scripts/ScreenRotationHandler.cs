﻿using UnityEngine;
using System.Collections;

public class ScreenRotationHandler {

    public void SetScreenOrientationAutoRotation()
    {
        Screen.orientation = ScreenOrientation.AutoRotation;
    }

    public void SetScreenOrientationLandscapeLeft()
    {
        Screen.orientation = ScreenOrientation.LandscapeLeft;
    }

    public void SetScreenOrientationLandscapeRight()
    {
        Screen.orientation = ScreenOrientation.LandscapeRight;
    }

    public void SetScreenOrientationPortrait()
    {
        Screen.orientation = ScreenOrientation.Portrait;
    }

    public void SetScreenOrientationPortraitUpsideDown()
    {
        Screen.orientation = ScreenOrientation.PortraitUpsideDown;
    }

}
