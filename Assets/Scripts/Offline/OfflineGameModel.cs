﻿using UnityEngine;
using System.Collections;
using TrueSync;
using UnityEngine.Networking;

public class OfflineGameModel{

    //Fürs Kippen des Bildschirms zuständing
    private ScreenRotationHandler screenRotationHandler;

    protected GameObject playerInstance;// Player deklarieren

    // AI deklarieren
    protected GameObject AIInstance1, AIInstance2, AIInstance3;

    protected GameObject ballInstance;// Ball deklarieren
    
    //Positionen der Spieler festlegen
    public Vector2 player1pos = new Vector2(-1.82f,-35.08f);
    public Vector2 player2pos = new Vector2(-30.17f, -3.54f);
    public Vector2 player3pos = new Vector2(-1.82f, 35.08f);
    public Vector2 player4pos = new Vector2(30.17f, -3.54f);

    public int player_anzahl = 0; /* wird Player nach dem 
    Ertellen als Parameter mit gegeben*/

    public OfflineGameModel()
    {
        screenRotationHandler = new ScreenRotationHandler();
        //Screen fixieren
        screenRotationHandler.SetScreenOrientationLandscapeLeft();
        // init GameObjects
		ballInstance = (GameObject)Resources.Load("GBall");
		playerInstance = (GameObject)Resources.Load("GPLayerHuman");
		AIInstance1 = (GameObject)Resources.Load("GPLayerAI");
		AIInstance2 = (GameObject)Resources.Load("GPLayerAI");
		AIInstance3 = (GameObject)Resources.Load("GPLayerAI");
    }

    public virtual void InstantiatePlayer()
    {
        //Spieler erstellen und einen Parameter übergeben
        playerInstance = (GameObject)GameObject.Instantiate
            (playerInstance, player1pos , new Quaternion(0, 0, 0, 0));
        playerInstance.GetComponent<OfflinePlayer>().SetPlayerNr(++player_anzahl);
    }
    public virtual void InstantiateAI()
    {
        //AI erstellen und einen Parameter übergeben
        AIInstance1 = (GameObject)GameObject.Instantiate
            (AIInstance1, player2pos, new Quaternion(0, 0, 0, 0));
        AIInstance1.GetComponent<OfflinePlayer>().SetPlayerNr(++player_anzahl);

        AIInstance2 = (GameObject)GameObject.Instantiate
            (AIInstance2, player3pos, new Quaternion(0, 0, 0, 0));       
        AIInstance2.GetComponent<OfflinePlayer>().SetPlayerNr(++player_anzahl);

        AIInstance3 = (GameObject)GameObject.Instantiate
            (AIInstance3, player4pos, new Quaternion(0, 0, 0, 0));    
        AIInstance3.GetComponent<OfflinePlayer>().SetPlayerNr(++player_anzahl);
    }
    public virtual void InstantiateBall()
    {
        //Ball erstellen
        ballInstance = (GameObject)GameObject.Instantiate
            (ballInstance, new Vector2(-2.35f, -1.32f), new Quaternion(0, 0, 0, 0));
    }

    public virtual void LeaveGame()
    {
        GameObject.Destroy(playerInstance);
        Application.Quit();
    }
}
