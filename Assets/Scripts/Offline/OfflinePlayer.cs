﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class OfflinePlayer : NetworkBehaviour{

	private Rigidbody2D rigidBody = null;
    private CircleCollider2D circleCollider = null;
	private int debugcnt = 0;

    public Sprite[] PlayerSprites;

    protected GameObject HUD_ref;
    protected Text LP_ref;
    protected GameObject Goal_ref;
    protected GameObject Ball_ref;
    protected GameObject Cam_ref;
    protected OfflineGameModel gameModel= null;
    public int playerNr;
    
    
    [SyncVar]
    public int lifepoints = 3;
    
    


    public OfflinePlayer ()
    {

 
    }

    public void SetPlayerNr(int playerNr)
    {
        this.playerNr = playerNr;
    }

   

	public virtual void Start()
    {
        //gamecontroller = GameObject.Find("GGameController");
        //Debug.Log("ich bin der "+gamecontroller.get_playerNr()+" spieler");
        HUD_ref = GameObject.Find("GCnvGameView");
        //Goal_ref = (GameObject)Instantiate(((GameObject)Resources.Load("GGoal", typeof(GameObject))), transform.position, Quaternion.identity);
        //Goal_ref.GetComponent<Goal>().Set_player(this);
        Ball_ref = GameObject.FindWithTag("ball");
        Cam_ref = GameObject.Find("GCamMain");

        switch (playerNr)
        {
            case 1:
                Goal_ref = GameObject.Find("GGoal1");
                Goal_ref.GetComponent<Goal>().Set_player(this);
                Cam_ref.GetComponent<CameraFollow>().set_playerCamera(this); // MainCam konzentriert sich auf spieler1
                /*Goal_ref.transform.position = new Vector3(-1.8f, -59.1f, 0);//erstelle in den nächsten Zeilen Codes für alle spieler ein Tor an der richtigen Position
                Goal_ref.transform.rotation = Quaternion.Euler(0f, 0f, 181.6f);
                Goal_ref.transform.localScale = new Vector3(24.711f, 22.29f, 1);*/
                break;
		    case 2:
                GetComponent<SpriteRenderer>().sprite = PlayerSprites[playerNr-2];
                Goal_ref = GameObject.Find("GGoal2");
                Goal_ref.GetComponent<Goal>().Set_player(this);
		    	LP_ref = GameObject.Find ("TxPl2LP").GetComponent<Text> ();
		    	/*Goal_ref.transform.position = new Vector3 (-57.8f, -0.8f, 0);
		    	Goal_ref.transform.rotation = Quaternion.Euler (0f, 0f, 89.15f);
		    	Goal_ref.transform.localScale = new Vector3 (22.925f, 23.468f, 1f);*/
                break;
            case 3:
                GetComponent<SpriteRenderer>().sprite = PlayerSprites[playerNr - 2];
                Goal_ref = GameObject.Find("GGoal3");
                Goal_ref.GetComponent<Goal>().Set_player(this);
                LP_ref = GameObject.Find("TxPl3LP").GetComponent<Text>();
                /*Goal_ref.transform.position = new Vector3(0f, 52.4f, 0);
                Goal_ref.transform.rotation = Quaternion.Euler(0f, 0f, -0.3f);
                Goal_ref.transform.localScale = new Vector3(25.303f, 25.039f, 1);*/
                break;
            case 4:
                GetComponent<SpriteRenderer>().sprite = PlayerSprites[playerNr - 2];
                Goal_ref = GameObject.Find("GGoal4");
                Goal_ref.GetComponent<Goal>().Set_player(this);
                LP_ref = GameObject.Find("TxPl4LP").GetComponent<Text>();
                /*Goal_ref.transform.position = new Vector3(54.8f, -1.9f, 0);
                Goal_ref.transform.rotation = Quaternion.Euler(0f, 0f, -87.8f);
                Goal_ref.transform.localScale = new Vector3(21.326f, 23.883f, 1f);*/
                break;
        }
    }

    public virtual void FixedUpdate()
    {
        if(playerNr==1)
        {
            HUD_ref.GetComponent<HUD>().HeartUI.sprite = HUD_ref.GetComponent<HUD>().HeartSprites[lifepoints]; // Lifepoints durch Herzen representieren
            if (lifepoints == 0)
                SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
                
        else
            LP_ref.text = "P" + playerNr+ " LP:"+lifepoints;

        if (lifepoints > 3) // lifepoints eingrenzen
            lifepoints = 3;
        if (lifepoints < 0)
            lifepoints = 0;
        if (lifepoints ==0)
        {

            Goal_ref.GetComponent<SpriteRenderer>().sprite =  Goal_ref.GetComponent<Goal>().goal_sprite;
            Goal_ref.GetComponent<PolygonCollider2D>().isTrigger = false;
            Destroy(gameObject);
        }
            
            
    }

    public virtual void Tor_kassiert()
    {
        Debug.Log("funktion: Tor_kassiert aufgerufen");
        if (lifepoints > 0)
            lifepoints -= 1;

        Ball_ref.GetComponent<BallReset>().Pos_Reset();
    }

	public GameObject GetGoal()
	{
		return this.Goal_ref;
	}
		
}
