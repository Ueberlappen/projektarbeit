﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour {


    private Vector2 velocity;

    public float smoothTime;

    public MonoBehaviour player;

	public void set_playerCamera(MonoBehaviour player)
    {
        this.player = player;

    }


	
	// Update is called once per frame
	void Update () {

        if (player != null)
        {
            float posX = Mathf.SmoothDamp(transform.position.x, player.transform.position.x, ref velocity.x, smoothTime);
            float posY = Mathf.SmoothDamp(transform.position.y, player.transform.position.y, ref velocity.y, smoothTime);

            transform.position = new Vector3(posX, posY, transform.position.z);

        }
        
	}
}
