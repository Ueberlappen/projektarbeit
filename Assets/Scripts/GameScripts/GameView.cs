﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameView : MonoBehaviour
{

    [Header("OptionPanel")]
    public GameObject pnOptions;
    public Button btnOptions;
    public Button btnResumeGame;
    public Button btnLeaveGame;
    public Slider sldVolume;
    public Text txtVolume;

    [Header("MapPanel")]
    public Camera camMap;

    [Header("LifePanel")]
    public GameObject pnLife;

    [Header("LifeEnemyPanel")]
    public GameObject pnLifeEnemy;
    public Button btnLifeEnemy;

    [Header("SpecialPanel")]
    public GameObject pnSpecial;
    public Button btnSpecial;

    /**
     * Get methods OptionPanel
     **/
    public GameObject GetPnOptions()
    {
        return pnOptions;
    }

    public Button GetBtnOptions()
    {
        return btnOptions;
    }

    public Button GetBtnResumeGame()
    {
        return btnResumeGame;
    }

    public Button GetBtnLeaveGame()
    {
        return btnLeaveGame;
    }

    public Slider GetSldVolume()
    {
        return sldVolume;
    }

    public Text GetTxtdVolume()
    {
        return txtVolume;
    }

    /**
     * Get methods MapPanel
     **/
    public Camera GetCamMap()
    {
        return camMap;
    }

    /**
    * Get methods LifePanel
    **/
    public GameObject GetPnLife()
    {
        return pnLife;
    }

    /**
    * Get methods SpecialPanel
    **/
    public GameObject GetPnLifeEnemy()
    {
        return pnLifeEnemy;
    }

    public Button GetBtnLifeEnemy()
    {
        return btnLifeEnemy;
    }

    /**
    * Get methods SpecialPanel
    **/
    public GameObject GetPnSpecial()
    {
        return pnSpecial;
    }

    public Button GetBtnSpecial()
    {
        return btnSpecial;
    }

}
