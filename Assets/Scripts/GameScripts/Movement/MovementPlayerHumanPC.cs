﻿using UnityEngine;
using System.Collections;

public class MovementPlayerHumanPC : MovementPlayer
{

    private int fingerID;
    private bool isPicking;
    private CircleCollider2D circleCollider = null;
    private Rigidbody2D rigidBody = null;


    // Use this for initialization
    void Start()
    {

        rigidBody = gameObject.GetComponent<Rigidbody2D>();
        circleCollider = gameObject.GetComponent<CircleCollider2D>();
        //rigidBody.gravityScale = 0;
        rigidBody.interpolation = RigidbodyInterpolation2D.Interpolate; // wird im Editor falsch angezeigt
        
       
    }

#if UNITY_EDITOR || UNITY_STANDALONE || UNITY_WEBPLAYER
    void OnMouseDown()// Funktion wird aufgerufen wenn linke Maustaste gedrückt wird
    {
        //püft ob Spieler-Colider == Mausposition und isPicking== false
        if (circleCollider.bounds.Contains((Vector2)Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 10f))) && !isPicking)
        {

            isPicking = true;
        }

    }


    void OnMouseDrag()// Funktion wird aufgerufen wenn wenn linke Maustaste gedrückt und Maus in Bewegung
    {
        if (isPicking) //&& IsDragableArea(Input.mousePosition))
        {
            //Camera.main.GetComponent<Transform>().localPosition = new Vector3 (0,0,-60);
            //rigidBody.MovePosition(controllerStrategy.move());//setze Spieler-Position gleich Mausposition
            //Debug.Log("Input.mousePosition: " + Input.mousePosition);
            //ebug.Log("Camera.main.ScreenToWorldPoint: " + Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x * 10, Input.mousePosition.y * 10, Camera.main.nearClipPlane)));

            rigidBody.MovePosition(Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 10f)));//setze Spieler-Position gleich Mausposition

        }
    }

    void OnMouseUp()// Funktion wird aufgerufen wenn wenn linke Maustaste los gelassen
    {
        isPicking = false;

    }
#endif
}
