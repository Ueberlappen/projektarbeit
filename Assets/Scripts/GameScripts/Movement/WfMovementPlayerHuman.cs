﻿using UnityEngine;
using System.Collections;

public class WfMovementPlayerHuman : MovementPlayer {

	private int fingerID;
	private bool isPicking;
	private CircleCollider2D circleCollider = null;
	private Vector3 movement;


	// Use this for initialization
	void Start () {


		circleCollider = gameObject.GetComponent<CircleCollider2D>();


	}



	// Update is called once per frame
	void Update () {

		#if UNITY_IPHONE || UNITY_ANDROID
		if (Input.touchCount > 0)
		{
			Touch[] touches = Input.touches;
			for (int i = 0; i < Input.touchCount; i++)
			{
				Touch touch = touches[i];
				if (touch.phase == TouchPhase.Began)
				{

					if (circleCollider.bounds.Contains((Vector2)Camera.main.ScreenToWorldPoint(new Vector3(touch.position.x, touch.position.y, 10f))) && !isPicking)
						//(circleCollider.bounds.Contains((Vector2)Camera.main.ScreenToWorldPoint(touch.position)) && !isPicking)
					{
						//rigidBody.velocity = Vector2.zero;  TODO: IBO Fragen
						isPicking = true;
						fingerID = touch.fingerId;
					}
				}
				else if (touch.phase == TouchPhase.Moved && touch.fingerId == fingerID)
				{
					if (isPicking)
					{
						movement = Camera.main.ScreenToWorldPoint(new Vector3(touch.position.x, touch.position.y, 10f));
					}
				}
				else if (touch.phase == TouchPhase.Ended && isPicking && touch.fingerId == fingerID)
				{
					isPicking = false;
					fingerID = -1;
				}
			}
		}

		#endif
	}

	#if UNITY_EDITOR || UNITY_STANDALONE || UNITY_WEBPLAYER
	void OnMouseDown()// Funktion wird aufgerufen wenn linke Maustaste gedrückt wird
	{

		//püft ob Spieler-Colider == Mausposition und isPicking== false
		if (circleCollider.bounds.Contains((Vector2)Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, - Camera.main.transform.position.z))) && !isPicking)
		{
			isPicking = true;
		}

	}


	void OnMouseDrag()// Funktion wird aufgerufen wenn wenn linke Maustaste gedrückt und Maus in Bewegung
	{
		if (isPicking) //&& IsDragableArea(Input.mousePosition))
		{
			movement = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, -Camera.main.transform.position.z));//setze Spieler-Position gleich Mausposition
		}
	}

	void OnMouseUp()// Funktion wird aufgerufen wenn wenn linke Maustaste los gelassen
	{
		isPicking = false;

	}
	#endif

	public Vector3 GetMovement()
	{
		return movement;
	}

	public bool IsPicking()
	{
		return isPicking;
	}
}
