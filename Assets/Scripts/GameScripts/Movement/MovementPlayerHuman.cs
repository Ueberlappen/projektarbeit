﻿using UnityEngine;
using System.Collections;



public class MovementPlayerHuman : MovementPlayer
{

    private int fingerID;
    private bool isPicking;
    private CircleCollider2D circleCollider = null;
    private Rigidbody2D rigidBody = null;
    

	// Use this for initialization
	void Start () {

        rigidBody= gameObject.GetComponent<Rigidbody2D>();
       
        if ( gameObject.GetComponent<OfflinePlayer>().playerNr== 1)
        {
            circleCollider = transform.Find("GPlayFingerCollider").gameObject.GetComponent<CircleCollider2D>();
        }

	}
	
   

	// Update is called once per frame
	void Update () {
		
#if UNITY_IPHONE || UNITY_ANDROID // folgender Code nur für Android-Phones gültig
        if (Input.touchCount > 0) //detektiert einen touch
        {
            Touch[] touches = Input.touches;
            for (int i = 0; i < Input.touchCount; i++)
            {
                Touch touch = touches[i];
                if (touch.phase == TouchPhase.Began) //der erste Augenblick nach einem touch
                {
                    /*Abfrage ob der circleCollider des spieler angetippt wurde und
                    isPicking==fale*/
                    if (circleCollider.bounds.Contains((Vector2)Camera.main.ScreenToWorldPoint
                        (new Vector3(touch.position.x, touch.position.y, 10f))) && !isPicking)
                    {
                        rigidBody.velocity = Vector2.zero;//Geschwindikeit = 0
                        isPicking = true;
                        fingerID = touch.fingerId; /*fingerID wird auf dem momentanen Stand
                        des Projects nicht gebraucht, es ermöglich aber Multitouch-Detektion*/
                    }
                }
                //Abfrage ob der Finger des Users sich über den Bilschirm bewegt
                else if (touch.phase == TouchPhase.Moved && touch.fingerId == fingerID)
                {
                    if (isPicking)
                    {
                        //Spielfigur foglt dem Finger des Users
                        rigidBody.MovePosition(Camera.main.ScreenToWorldPoint
                            (new Vector3(touch.position.x, touch.position.y, 10f)));
                    }
                }
                //Abfrage ob der User den Finger weg nimmt
                else if (touch.phase == TouchPhase.Ended && isPicking && touch.fingerId == fingerID)
                {
                    isPicking = false;
                    fingerID = -1;
                }
            }
        }

#endif
    }

#if UNITY_EDITOR || UNITY_STANDALONE || UNITY_WEBPLAYER // folgender Code für Computer-Rechner gültig
    void OnMouseDown()// Funktion wird aufgerufen wenn linke Maustaste gedrückt wird
    {
        //püft ob Spieler-Colider == Mausposition und isPicking== false
        if (circleCollider.bounds.Contains((Vector2)Camera.main.ScreenToWorldPoint
(new Vector3(Input.mousePosition.x, Input.mousePosition.y, - Camera.main.transform.position.z))) && !isPicking)
        {
            isPicking = true;
        }
    }


    void OnMouseDrag()// Funktion wird aufgerufen wenn wenn linke Maustaste gedrückt und Maus in Bewegung
    {
        if (isPicking)
        {
            //Spielfigur foglt Mausposition
            rigidBody.MovePosition(Camera.main.ScreenToWorldPoint
(new Vector3(Input.mousePosition.x, Input.mousePosition.y, -Camera.main.transform.position.z))); 
        }
    }

    void OnMouseUp()// Funktion wird aufgerufen wenn wenn linke Maustaste los gelassen wird
    {
        isPicking = false;
    }

	#endif
}
