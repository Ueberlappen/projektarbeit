using UnityEngine;
using System.Collections;



public class BtMovementPlayerHuman : MovementPlayer
{

    private int fingerID;
    private bool isPicking;
    private CircleCollider2D circleCollider = null;
    private Rigidbody2D rigidBody = null;
    

	// Use this for initialization
	void Start () {

        if(!isLocalPlayer){
            return;
        }
        rigidBody= gameObject.GetComponent<Rigidbody2D>();
        circleCollider = transform.Find("GPlayFingerCollider").gameObject.GetComponent<CircleCollider2D>();
	}
	
	void Update () {

        if(!isLocalPlayer){
            return;
        }
        if (Input.touchCount > 0)
        {
            Touch[] touches = Input.touches;
            for (int i = 0; i < Input.touchCount; i++)
            {
                Touch touch = touches[i];
                if (touch.phase == TouchPhase.Began)
                {

                    if (circleCollider.bounds.Contains((Vector2)Camera.main.ScreenToWorldPoint(new Vector3(touch.position.x, touch.position.y, 10f))) && !isPicking)
                    //(circleCollider.bounds.Contains((Vector2)Camera.main.ScreenToWorldPoint(touch.position)) && !isPicking)
                    {
                        rigidBody.velocity = Vector2.zero;
                        isPicking = true;
                        fingerID = touch.fingerId;
                    }
                }
                else if (touch.phase == TouchPhase.Moved && touch.fingerId == fingerID)
                {
                    if (isPicking)
                    {
                        rigidBody.MovePosition(Camera.main.ScreenToWorldPoint(new Vector3(touch.position.x, touch.position.y, 10f)));
                    }
                }
                else if (touch.phase == TouchPhase.Ended && isPicking && touch.fingerId == fingerID)
                {
                    isPicking = false;
                    fingerID = -1;
                }
            }
        }
    }
}
