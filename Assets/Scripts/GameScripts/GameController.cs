﻿using UnityEngine;
using System.Collections;
using TrueSync;
using UnityEngine.Networking;

public class GameController : NetworkBehaviour
{
	public GameView gameView;
    public GameObject IngameButtonHolder, OptionsButtonholder;

    private OfflineGameModel gameModel;
	
	void Start ()
	{
		switch (GameModeSettings.gameMode) 
		{
		case EGameMode.GameModeOffline:
			gameModel = new OfflineGameModel ();
            gameModel.InstantiatePlayer ();
            gameModel.InstantiateAI();
            gameModel.InstantiateBall ();
			break;
		case EGameMode.GameModeBluetooth:
			gameModel = new BtGameModel ();
			gameModel.InstantiatePlayer ();
            gameModel.InstantiateBall ();
			Debug.Log ("GameController: \"switch(gameModel = new BluetoothGameModel()\"");
			break;
		case EGameMode.GameModeWifi:
			gameModel = new WifiGameModel (this.GetComponent<TrueSyncManager> ());
			gameModel.InstantiatePlayer ();
            gameModel.InstantiateBall ();
			Debug.Log ("GameController: \"switch(gameModel = new Wifi()\"");
			break;
		default:
			gameModel = new OfflineGameModel ();
			gameModel.InstantiatePlayer ();
            gameModel.InstantiateAI();
            gameModel.InstantiateBall ();
			Debug.Log ("GameController: \"switch DEFAULT(gameModel = new OfflineGameModel()\"");
			break;
		}
	}

    int get_playerNr()
    {
        int i;

        i = gameModel.player_anzahl;

        return i;
    }

	// Update is called once per frame
	void Update ()
	{
		if (gameModel != null) {
			//gameModel.NextStep ();
		}

	}



	public void OnClickOptionsButton ()
	{
		gameView.GetPnOptions ().SetActive (true);
	}

	public void OnClickResumeGameButton ()
	{
		gameView.GetPnOptions ().SetActive (false);
	}

	public void OnClickLeaveGameButton ()
	{
		gameModel.LeaveGame ();
	}

	public void OnChangedValueVolumeSlider ()
	{

	}

	public void OnClickLifeEnemyButton ()
	{

	}

	public void OnClickSpecialButton ()
	{

	}
	
        public void OnClickExitButton()
        {
            Application.Quit();
        }

    
    // ab jetzt Funktionen für Ingame-Menu
      

        public void OptionMenu()// wenn Option-Button gedrückt
        {
            OptionsButtonholder.SetActive(true); //Layout für Optionen sind sichtbar
            IngameButtonHolder.SetActive(false); //Layout für Ingame-Button sind unsichtbar
        }

        public void Event(){}

        public void Resume() // wenn Resume-Button gedrückt
        {
            OptionsButtonholder.SetActive(false); //Layout für Optionen sind unsichtbar
            IngameButtonHolder.SetActive(true); //Layout für Ingame-Button sind sichtbar

        }

        public void Volume()
        {

        }

        public void MainMenu()
        {
            Application.LoadLevel("SceneMainMenu");
        }

        public void Exit()
        {
            Application.Quit();
        }

}
