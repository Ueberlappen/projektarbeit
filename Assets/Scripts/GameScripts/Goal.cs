﻿using UnityEngine;
using System.Collections;


public class Goal : MonoBehaviour {

    protected OfflinePlayer player;
    public Sprite goal_sprite;

    public void Set_player(OfflinePlayer player)
    {
        //Debug.Log ("UnityBluetooth: \tGoal:\t SetPlayer - PlayerID: "+player.btPlayerID+" \n");
        this.player = player;
    }
    
    public void SetMyGoalColor()
    {
        Debug.Log ("UnityBluetooth: \tGoal:\t SetMyGoalColor - wird ausgeführt \n");
        gameObject.GetComponent<SpriteRenderer>().color = new Color(0.5f,0.5f,0.5f,1.0f);
        /*
        GameObject whateverGameobject = whatever;
        Color whaterverColor = new Color(RValue,GValue,BValue,1);
        
        MeshRenderer gameObjectRender = whateverGameobject.GetComponent<MeshRenderer>();
        Material newMaterial = new Material(Shader.Find("Whatever name of shater you want to use"));
        
        newMaterial.color = whateverColor;
        gameObjectRender.material = newMaterial;
        */
    }

	// Use this for initialization
	void Start () 
	{
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter2D(Collider2D other)
    {

        Debug.Log("collision detected");
        if (other.gameObject.CompareTag("ball"))
        {
            Debug.Log("Ball collision detected");
            player.Tor_kassiert();
          
           //Destroy(player);
        }
    }
}
