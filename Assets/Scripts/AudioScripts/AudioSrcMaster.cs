﻿using UnityEngine;
using System.Collections;

public class AudioSrcMaster : MonoBehaviour {

    private static AudioSrcMaster instance = null;
    public static AudioSrcMaster Instance
    {
        get { return instance; }
    }

    public AudioSource GameObjectInstance
    {
        get { return this.gameObject.GetComponent<AudioSource>(); }
    }
    void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
            return;
        }
        else
        {
            instance = this;
        }
        DontDestroyOnLoad(this.gameObject);
    }
}
