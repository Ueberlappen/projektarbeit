﻿using UnityEngine;
using System.Collections;

public class AudioSrcSFX : MonoBehaviour {

    private static AudioSrcSFX instance = null;
    public static AudioSrcSFX Instance
    {
        get { return instance; }
    }
    public AudioSource GameObjectInstance
    {
        get { return this.gameObject.GetComponent<AudioSource>(); }
    }
    void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
            return;
        }
        else
        {
            instance = this;
        }
        DontDestroyOnLoad(this.gameObject);
    }
}
