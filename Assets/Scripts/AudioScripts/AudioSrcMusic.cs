﻿using UnityEngine;
using System.Collections;

public class AudioSrcMusic : MonoBehaviour
{

    private static AudioSrcMusic instance = null;
    public static AudioSrcMusic Instance
    {
        get { return instance; }
    }
    public AudioSource GameObjectInstance
    {
        get { return this.gameObject.GetComponent<AudioSource>(); }
    }
    void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
            return;
        }
        else
        {
            instance = this;
        }
        DontDestroyOnLoad(this.gameObject);
    }
}
