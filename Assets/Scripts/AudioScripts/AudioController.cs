﻿using UnityEngine;
using System.Collections;

public class AudioController : MonoBehaviour {

    public MainMenuView mainMenuView;
    public AudioSource audioSrcMusic;
    public AudioSource audioSrcSFX;

    public AudioClip audioClpButton;
    public AudioClip audioClpSlider;

    private static float sldOptionsMainMasterVol = 5;
	private static float sldOptionsMainMusicVol = 5;
	private static float sldOptionsMainSFXVol = 5;


    void Start()
    {
        audioSrcMusic = AudioSrcMusic.Instance.GameObjectInstance;
        audioSrcSFX = AudioSrcSFX.Instance.GameObjectInstance;

		mainMenuView.sldOptionsMainMasterVol.value = sldOptionsMainMasterVol;
		mainMenuView.sldOptionsMainMusicVol.value = sldOptionsMainMusicVol;
		mainMenuView.sldOptionsMainSFXVol.value = sldOptionsMainSFXVol;

		audioSrcMusic.volume = (mainMenuView.sldOptionsMainMusicVol.value / 10) * (mainMenuView.sldOptionsMainMasterVol.value / 10);
		audioSrcSFX.volume = (mainMenuView.sldOptionsMainSFXVol.value / 10) * (mainMenuView.sldOptionsMainMasterVol.value / 10);

		
    }

    public void OnClickBtn()
    {
        audioSrcSFX.PlayOneShot(audioClpButton);
    }

    public void OnChangedValueSldOptionsMainMasterVol()
    {
        sldOptionsMainMasterVol = (mainMenuView.sldOptionsMainMasterVol.value);
        audioSrcMusic.volume = (mainMenuView.sldOptionsMainMusicVol.value / 10) * (mainMenuView.sldOptionsMainMasterVol.value / 10);
        audioSrcSFX.volume = (mainMenuView.sldOptionsMainSFXVol.value / 10) * (mainMenuView.sldOptionsMainMasterVol.value / 10);
        audioSrcSFX.PlayOneShot(audioClpSlider);
    }

    public void OnChangedValueSldOptionsMainMusicVol()
    {
		sldOptionsMainMusicVol = (mainMenuView.sldOptionsMainMusicVol.value);
        audioSrcMusic.volume = (mainMenuView.sldOptionsMainMusicVol.value / 10) * (mainMenuView.sldOptionsMainMasterVol.value / 10);
        audioSrcSFX.PlayOneShot(audioClpSlider);
    }

    public void OnChangedValueSldOptionsMainSFXVol()
    {
		sldOptionsMainSFXVol = (mainMenuView.sldOptionsMainSFXVol.value);
        audioSrcSFX.volume = (mainMenuView.sldOptionsMainSFXVol.value / 10) * (mainMenuView.sldOptionsMainMasterVol.value / 10);
        audioSrcSFX.PlayOneShot(audioClpSlider);
    }
}
