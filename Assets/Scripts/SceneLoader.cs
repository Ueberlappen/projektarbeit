﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class SceneLoader : MonoBehaviour{

	public bool disconnect;

	public void Load(string scene) {
		if (disconnect) {
			PhotonNetwork.Disconnect ();
		}
		SceneManager.LoadScene (scene);
	}
}
