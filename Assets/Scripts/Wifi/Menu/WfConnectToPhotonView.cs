﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;


public class WfConnectToPhotonView : MonoBehaviour{

    [Header("NamePanel")]
    public GameObject pnWfMenuName;
    public InputField inFdPlayerName;

    [Header("ConnectingInfoPanel")]
    public Text     pnWfMenuInfo;
    public Button   btnErrorPerceived;

    [Header("LobbyPanel")]
    public GameObject    pnWfMenuLobby;
    public Text          txtRoomInfo;
    public RectTransform pnRoomList;
    public GameObject    btnJoinRoom;

    [Header("RoomPanel")]
    public GameObject    pnWfMenuRoom;
    public Transform[]   pnPlayerList;
    public Button        btnStartGame;
    public Text          txtTimerInfo;

    public GameObject InstantiateBtnJoinRoom(GameObject btn)
    {
         return Instantiate(btn);
    }
    public void DestroyBtnJoinRoom(GameObject btn)
    {
        Destroy(btn);
    }
    public void SortBtnJoinRoom(int openRoomsCounter)
    {
        int currentMatchesCount = pnRoomList.transform.childCount;

        if (openRoomsCounter >= currentMatchesCount)
        {
            for (int index = 0; index < (openRoomsCounter - currentMatchesCount); index++)
            {
                GameObject newMatchBtn = InstantiateBtnJoinRoom(btnJoinRoom);
                newMatchBtn.transform.SetParent(pnRoomList, false);
            }
        }
        else
        {
            for (int index = 0; index < (currentMatchesCount - openRoomsCounter); index++)
            {
                DestroyBtnJoinRoom(pnRoomList.transform.GetChild(currentMatchesCount - (index + 1)).gameObject);
            }
        }
        pnRoomList.sizeDelta = new Vector2(pnRoomList.sizeDelta.x, openRoomsCounter * btnJoinRoom.GetComponent<RectTransform>().sizeDelta.y);
    }
    public void NameBtnJoinRoom(RoomInfo[] roomList)
    {
        for (int index = 0; index < roomList.Length; index++)
        {
            WfJoinRoom matchJoiner = pnRoomList.transform.GetChild(index).GetComponent<WfJoinRoom>();
            matchJoiner.UpdateRoom(roomList[index]);
        }
    }

    public void ClearPnPlayerList()
    {
        foreach (Transform playerBox in pnPlayerList)
        {
            playerBox.GetComponent<Image>().enabled = false;
            playerBox.FindChild("PlayerNameText").GetComponent<Text>().text = "";
        }
    }
    public void UpdatePnPlayerList(PhotonPlayer[] playerList)
    {
        for (int index = 0; index < playerList.Length; index++)
        {
            Transform playerBox = pnPlayerList[index];
            playerBox.GetComponent<Image>().enabled = true;

            Text playerNameText = playerBox.FindChild("PlayerNameText").GetComponent<Text>();
            playerNameText.text = playerList[index].NickName.Trim();
        }
    }
}
