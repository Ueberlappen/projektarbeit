﻿using UnityEngine;
using System.Collections;

public interface IObservable{

    void AddObserver(Observer o);

}
