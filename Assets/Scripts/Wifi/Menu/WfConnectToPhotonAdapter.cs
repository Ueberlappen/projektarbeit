﻿using UnityEngine;
using System.Collections;
using System;

public class WfConnectToPhotonAdapter : Observer
{
    private WfConnectToPhotonView connectToPhotonView;
    private WfConnectToPhotonModel connectToPhotonModel;

    public enum PanelType { Info, Error, Name, Lobby, Room, Count };

    public WfConnectToPhotonAdapter(WfConnectToPhotonView view, WfConnectToPhotonModel model)
    {
        this.connectToPhotonView = view;
        this.connectToPhotonModel = model;
    }

    public void RegisterEvents()
    {
        connectToPhotonModel.AddObserver(this);
        connectToPhotonModel.Timer.AddObserver(this);
        connectToPhotonModel.StateStart.AddObserver(this);
        connectToPhotonModel.StateName.AddObserver(this);
        connectToPhotonModel.StateConnecting.AddObserver(this);
        connectToPhotonModel.StateError.AddObserver(this);
        connectToPhotonModel.StateLobby.AddObserver(this);
        connectToPhotonModel.StateDisconnecting.AddObserver(this);
        connectToPhotonModel.StateCreatingRoom.AddObserver(this);
        connectToPhotonModel.StateRoomAsMaster.AddObserver(this);
        connectToPhotonModel.StateRoomAsClient.AddObserver(this);
        connectToPhotonModel.StateLeavingRoom.AddObserver(this);
        connectToPhotonModel.StateStartingGame.AddObserver(this);
    }
    public void update(IObservable o, string message)
    {
        if(o == connectToPhotonModel)
        {
            if (connectToPhotonView.pnWfMenuLobby.activeInHierarchy == true)
            {
                connectToPhotonView.txtRoomInfo.text = message;
                connectToPhotonView.SortBtnJoinRoom(connectToPhotonModel.PUNManager.RoomList.Length);
                connectToPhotonView.NameBtnJoinRoom(connectToPhotonModel.PUNManager.RoomList);
            }
            else
            {
                connectToPhotonView.ClearPnPlayerList();
                connectToPhotonView.UpdatePnPlayerList(connectToPhotonModel.PUNManager.GetPlayerList());
            }  
        }
        if(o == connectToPhotonModel.Timer)
        {
            connectToPhotonView.txtTimerInfo.text = message;
        }

        if(o == connectToPhotonModel.StateName)
        {
            ActivePanel(PanelType.Name);
        }

        if (o == connectToPhotonModel.StateConnecting || o == connectToPhotonModel.StateDisconnecting || o == connectToPhotonModel.StateCreatingRoom || o == connectToPhotonModel.StateLeavingRoom)
        {
            connectToPhotonView.pnWfMenuInfo.text = message;
            ActivePanel(PanelType.Info);
        }

        if (o == connectToPhotonModel.StateError)
        {
            connectToPhotonView.pnWfMenuInfo.text = message;
            ActivePanel(PanelType.Error);
        }

        if (o == connectToPhotonModel.StateLobby)
        {
            ActivePanel(PanelType.Lobby);
        }
     
        if (o == connectToPhotonModel.StateRoomAsMaster)
        {
            connectToPhotonView.btnStartGame.gameObject.SetActive(true);
            ActivePanel(PanelType.Room); 
        }

        if (o == connectToPhotonModel.StateRoomAsClient)
        {
            ActivePanel(PanelType.Room);
        }
        
        if (o == connectToPhotonModel.StateStartingGame)
        {
            connectToPhotonView.btnStartGame.gameObject.SetActive(false);
            connectToPhotonView.txtTimerInfo.gameObject.SetActive(true);
        }
    }

    private void ActivePanel(PanelType panelType)
    {
        connectToPhotonView.pnWfMenuName.SetActive(panelType == PanelType.Name ? true : false);
        connectToPhotonView.pnWfMenuInfo.gameObject.SetActive((panelType == PanelType.Info) || (panelType == PanelType.Error) ? true : false);
        connectToPhotonView.btnErrorPerceived.gameObject.SetActive(panelType == PanelType.Error ? true : false);
        connectToPhotonView.pnWfMenuLobby.SetActive(panelType == PanelType.Lobby ? true : false);
        connectToPhotonView.pnWfMenuRoom.SetActive(panelType == PanelType.Room ? true : false);
    }
}
