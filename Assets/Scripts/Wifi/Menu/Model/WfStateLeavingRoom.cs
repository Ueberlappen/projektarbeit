﻿using UnityEngine;
using System.Collections;

public class WfStateLeavingRoom : IWfState {

    public WfStateLeavingRoom(WfConnectToPhotonModel model) : base(model)
    {

    }

    public override void NextStep()
    {
        if (isEntryBlocked == false)
        {
            entry();
        }

        if ((connectToPhotonModel.IsPlayerInRoom() == false))
        {
            unblockEntry();
            connectToPhotonModel.SetStateLobby();
        }

        if (connectToPhotonModel.IsConnectionWithPhotonIssue() == true)
        {
            unblockEntry();
            connectToPhotonModel.SetStateError();
        }
    }

    private void entry()
    {
        SetChanged();
        notifyObservers("Leaving Room...");
        isEntryBlocked = true;
    }
    private void unblockEntry()
    {
        isEntryBlocked = false;
    }
}
