﻿using UnityEngine;
using System.Collections;

public class WfStateName : IWfState
{

    public WfStateName(WfConnectToPhotonModel model) : base(model)
    {

    }

    public override void NextStep()
    {
        if (isEntryBlocked == false)
        {
            entry();
        }
        
        if (connectToPhotonModel.IsNameApproved() == true)
        {
            connectToPhotonModel.SetUpPhoton();
            connectToPhotonModel.ConnectWithPhoton();

            unblockEntry();
            connectToPhotonModel.SetStateConnecting();
            connectToPhotonModel.NameApproved = false;
        }
    }

    private void entry()
    {
        SetChanged();
        NotifyObservers();
        isEntryBlocked = true;
    }
    private void unblockEntry()
    {
        isEntryBlocked = false;
    }
}
