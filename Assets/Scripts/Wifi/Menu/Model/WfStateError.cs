﻿using UnityEngine;
using System.Collections;

public class WfStateError : IWfState {

    public WfStateError(WfConnectToPhotonModel model) : base(model)
    {

    }

    public override void NextStep()
    {
        if (isEntryBlocked == false)
        {
            entry();
        }

        if (connectToPhotonModel.IsErrorPerceivedApproved() == true)
        {
            unblockEntry();
            connectToPhotonModel.SetStateStart();
            connectToPhotonModel.ErrorPerceivedApproved = false;
        }
    }

    private void entry()
    {
        SetChanged();
        notifyObservers("Please check your Wifi connection!");
        isEntryBlocked = true;
    }
    private void unblockEntry()
    {
        isEntryBlocked = false;
    }
}
