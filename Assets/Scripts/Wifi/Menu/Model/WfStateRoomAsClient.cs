﻿using UnityEngine;
using System.Collections;

public class WfStateRoomAsClient : IWfState {

    public WfStateRoomAsClient(WfConnectToPhotonModel model) : base(model)
    {

    }

    public override void NextStep()
    {
        if (isEntryBlocked == false)
        {
            entry();
        }

        if (connectToPhotonModel.IsLeaveRoomApproved() == true)
        {
            connectToPhotonModel.LeaveRoom();

            unblockEntry();
            connectToPhotonModel.SetStateLeavingRoom();
            connectToPhotonModel.LeaveRoomApproved = false;
        }
        if (connectToPhotonModel.IsMasterClientSwitched() == true)
        {
            if(connectToPhotonModel.IsMaster() == true)
            {
                unblockEntry();
                connectToPhotonModel.SetStateRoomAsMaster();
            }  
        }

        if (connectToPhotonModel.IsMasterStartedGame() == true)
        {
            connectToPhotonModel.StartTimer(3);

            unblockEntry();
            connectToPhotonModel.SetStateStartingGame();
           
        }

        if (connectToPhotonModel.IsConnectionWithPhotonIssue() == true)
        {
            unblockEntry();
            connectToPhotonModel.SetStateError();
        }

        connectToPhotonModel.UpdatePlayersInRoom();
    }

    private void entry()
    {
        SetChanged();
        NotifyObservers();
        isEntryBlocked = true;
    }
    private void unblockEntry()
    {
        isEntryBlocked = false;
    }
}
