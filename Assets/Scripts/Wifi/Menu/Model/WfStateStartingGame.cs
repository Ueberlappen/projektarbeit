﻿using UnityEngine;
using System.Collections;

public class WfStateStartingGame : IWfState {

    public WfStateStartingGame(WfConnectToPhotonModel model) : base(model)
    {

    }

    public override void NextStep()
    {
        if (isEntryBlocked == false)
        {
            entry();
        }

        if (connectToPhotonModel.IsTimerReady() == true)
        {
            unblockEntry();
            connectToPhotonModel.LoadLevel();
        }
        else
        {
            connectToPhotonModel.DecrementTimer();
        }

        if (connectToPhotonModel.IsConnectionWithPhotonIssue() == true)
        {
            unblockEntry();
            connectToPhotonModel.SetStateError();
        }
    }

    private void entry()
    {
        SetChanged();
        NotifyObservers();
        GameModeSettings.gameMode = EGameMode.GameModeWifi;
        isEntryBlocked = true;
    }
    private void unblockEntry()
    {
        isEntryBlocked = false;
    }
}
