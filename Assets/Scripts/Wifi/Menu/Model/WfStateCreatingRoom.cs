﻿using UnityEngine;
using System.Collections;

public class StateCreatingRoom : IWfState {

    public StateCreatingRoom(WfConnectToPhotonModel model) : base(model)
    {

    }

    public override void NextStep()
    {
        if (isEntryBlocked == false)
        {
            entry();
        }

        if (connectToPhotonModel.IsPlayerInRoom() == true)
        {
            unblockEntry();
            connectToPhotonModel.SetStateRoomAsMaster();   
        }

        if (connectToPhotonModel.IsConnectionWithPhotonIssue() == true)
        {
            unblockEntry();
            connectToPhotonModel.SetStateError();
        }
    }

    private void entry()
    {
        SetChanged();
        notifyObservers("Creating Match...");
        isEntryBlocked = true;
    }

    private void unblockEntry()
    {
        isEntryBlocked = false;
    }
}
