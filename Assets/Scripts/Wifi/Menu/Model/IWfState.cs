﻿using UnityEngine;
using System.Collections;

public abstract class IWfState : Observable{

    protected WfConnectToPhotonModel connectToPhotonModel;
    protected bool isEntryBlocked = false;
    
	public IWfState(WfConnectToPhotonModel model)
    {
        this.connectToPhotonModel = model;
    }

    public abstract void NextStep();
}
