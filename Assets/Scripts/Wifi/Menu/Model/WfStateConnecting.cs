﻿using UnityEngine;
using System.Collections;

public class StateConnecting : IWfState {

    public StateConnecting(WfConnectToPhotonModel model) : base(model)
    {

    }

    public override void NextStep()
    {
        if (isEntryBlocked == false)
        {
            entry();
        }

        if ((connectToPhotonModel.IsConnectedWithLobby() == true))
        {
            unblockEntry();
            connectToPhotonModel.SetStateLobby();    
        }

        if (connectToPhotonModel.IsConnectionWithPhotonIssue() == true)
        {
            unblockEntry();
            connectToPhotonModel.SetStateError();
        }
    }

    private void entry()
    {
        SetChanged();
        notifyObservers("Connecting...");
        isEntryBlocked = true;
    }

    private void unblockEntry()
    {
        isEntryBlocked = false;
    }
}
