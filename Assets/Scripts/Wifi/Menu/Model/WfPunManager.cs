﻿using UnityEngine;
using System.Collections;
using Photon;

public class WfPunManager : PunBehaviour
{
    private string lobbyName;
    private string levelToLoad;

    private bool connectedWithLobby = false;
    private bool connectionIssue = false;
    private bool masterClientSwitched = false;
    private bool playerInRoom = false;
    private bool clientJoinedLeavedRoom = false;
    private bool masterStartedGame = false;

    private RoomInfo[] roomList = null;

    public void Connect()
    {
        connectionIssue = false;
        PhotonNetwork.ConnectUsingSettings("v1.0");
    }
    public void Disconnect()
    {
        PhotonNetwork.Disconnect();
    }
    public void CreateLobby(string lobbyName)
    {
        this.lobbyName = lobbyName;
        PhotonNetwork.lobby = new TypedLobby(this.lobbyName, LobbyType.Default);
    }
    public void CreateRoom(string name)
    {
        PhotonNetwork.CreateRoom(name);
    }
    public void LeaveRoom()
    {
        PhotonNetwork.LeaveRoom();
    }
    public void SetRoomVisible(bool b)
    {
        PhotonNetwork.room.IsVisible = b;
    }
    public void InformClients()
    {
        photonView.RPC("StartMatch", PhotonTargets.All);
    }
    public void LoadGame()
    {
        PhotonNetwork.LoadLevel(this.levelToLoad);
    }

    public bool IsConnectedWithPhoton()
    {
        bool connectedWithPhoton = false;
        PhotonNetwork.CrcCheckEnabled = true;

        // Checks if it is already connected
        if (PhotonNetwork.connected)
        {
            OnReceivedRoomListUpdate();
            connectedWithPhoton = true;
        }
        return connectedWithPhoton;
    }
    public bool IsConnectedWithLobby
    {
        get { return connectedWithLobby; }
    }
    public bool IsConnectionWithPhotonIssue
    {
        get { return connectionIssue; }
    }
    public bool IsPlayerInRoom
    {
        get { return playerInRoom; }
    }
    public bool IsClientJoinedLeavedRoom
    {
        get { return clientJoinedLeavedRoom; }
    }
    public bool IsMasterClientSwitched
    {
        get { return masterClientSwitched; }
    }
    public bool IsMasterStartedGame
    {
        get { return masterStartedGame; }
    }
    public bool IsMaster()
    {
        return PhotonNetwork.isMasterClient;
    }

    public string LobbyName
    {
        get { return lobbyName; }
    }
    public string LevelToLoad
    {
        get { return levelToLoad; }
        set { levelToLoad = value; }
    }
    public string PlayerName
    {
        get { return PhotonNetwork.player.NickName; }
        set { PhotonNetwork.player.NickName = value;
        }
    }
    public int PlayerID
    {
        get { return PhotonNetwork.player.ID; }
    }
    public PhotonPlayer[] GetPlayerList()
    {
        return PhotonNetwork.playerList;
    }
    public RoomInfo[] RoomList
    {
        get { return roomList; }
    }

    public override void OnJoinedLobby()
    {
        connectedWithLobby = true;
    }
    public override void OnConnectedToMaster()
    {
        PhotonNetwork.JoinLobby(PhotonNetwork.lobby);
    }
    public override void OnLeftLobby()
    {
        connectedWithLobby = false;
        Debug.Log("OnLeftLobby");
    }
    public override void OnDisconnectedFromPhoton()
    {
        
    }
    public override void OnFailedToConnectToPhoton(DisconnectCause cause)
    {
        connectionIssue = true;
        Debug.Log("No Connection with Photon");
    }
    public override void OnConnectionFail(DisconnectCause cause)
    {
        connectionIssue = true;
        Debug.Log("No Connection");
    }
    public override void OnConnectedToPhoton()
    {
   
    }
    public override void OnReceivedRoomListUpdate()
    {
        Debug.Log("OnReceivedRoomListUpdate");
        roomList = PhotonNetwork.GetRoomList();
    }
    public override void OnJoinedRoom()
    {
        Debug.Log("Joined Room");
        masterClientSwitched = true;
        playerInRoom = true;
    }
    public override void OnLeftRoom()
    {
        playerInRoom = false;
    }
    public override void OnMasterClientSwitched(PhotonPlayer newMasterClient)
    {
        masterClientSwitched = true;
    }
    public override void OnPhotonPlayerConnected(PhotonPlayer newPlayer)
    {
        clientJoinedLeavedRoom = true;
        
    }
    public override void OnPhotonPlayerDisconnected(PhotonPlayer disconnetedPlayer)
    {
        clientJoinedLeavedRoom = true;
        
    }

    [PunRPC]
    public void StartMatch()
    {
        masterStartedGame = true;
        Debug.Log("PunRPC received");
    }
}
