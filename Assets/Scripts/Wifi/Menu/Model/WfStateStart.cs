﻿using UnityEngine;
using System.Collections;
using System;

public class WfStateStart : IWfState{

    
    public WfStateStart(WfConnectToPhotonModel model) : base(model)
    {
        
    }

    public override void NextStep()
    {
        connectToPhotonModel.SetStateName();
    }

}
