﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class WfConnectToPhotonModel : Observable{

    public static WfPunManager punManager;
    private ScreenRotationHandler screenOrientationHandler;
    private Timer timer;
    private string name;

    private IWfState state;
    private IWfState stateStart;
    private IWfState stateName;
    private IWfState stateConnecting;
    private IWfState stateError;
    private IWfState stateDisconnecting;
    private IWfState stateLobby;
    private IWfState stateCreatingRoom;
    private IWfState stateRoomAsClient;
    private IWfState stateRoomAsMaster;
    private IWfState stateLeavingRoom;
    private IWfState stateStartingGame;

    private bool nameApproved = false;
    private bool errorPerceivedApproved = false;
    private bool leaveLobbyApproved = false;
    private bool createRoomApproved = false;
    private bool startGameApproved = false;
    private bool leaveRoomApproved = false;

    public WfConnectToPhotonModel(WfPunManager punManager)
    {
        WfConnectToPhotonModel.punManager = punManager;
        screenOrientationHandler = new ScreenRotationHandler();
        screenOrientationHandler.SetScreenOrientationAutoRotation();
        timer = new Timer();
        name = null;
        stateStart = new WfStateStart(this);
        stateName = new WfStateName(this);
        stateConnecting = new StateConnecting(this);
        stateError = new WfStateError(this);
        stateDisconnecting = new WfStateDisconnecting(this);
        stateLobby = new WfStateLobby(this);
        stateCreatingRoom = new StateCreatingRoom(this);
        stateRoomAsClient = new WfStateRoomAsClient(this);
        stateRoomAsMaster = new WfStateRoomAsMaster(this);
        stateLeavingRoom = new WfStateLeavingRoom(this);
        stateStartingGame = new WfStateStartingGame(this);

        this.SetStateStart();
    }

    public void NextStep()
    {
        state.NextStep();
    }

    public void SetUpPhoton()
    {
        punManager.LevelToLoad = "Scenes/SceneGame";
        punManager.PlayerName = this.name;
        punManager.CreateLobby("demo_boxes");  
    }
    public void ConnectWithPhoton()
    {
        punManager.Connect();
        Debug.Log("Connect");
    }
    public void DisconnectFromPhoton()
    {
        punManager.Disconnect();
    }
    public void UpdateOpenRooms()
    {
        if (punManager.RoomList.Length == 0)
        {
            SetChanged();
            notifyObservers("No Matches Online");   
        }
        else
        {
            SetChanged();
            notifyObservers("Join Match");
        }
    }
    public void CreateRoom()
    {
        punManager.CreateRoom(name);
    }
    public void LeaveRoom()
    {
        punManager.LeaveRoom();
    }
    public void UpdatePlayersInRoom()
    {
        SetChanged();
        NotifyObservers();
    }
    public void CloseRoom()
    {
        punManager.SetRoomVisible(false);
         
    }
    public void InformClients()
    {
        punManager.InformClients();
    }
    public void StartTimer(int time)
    {
        timer.SetStart(time);
    }
    public void DecrementTimer()
    {
        timer.Decrement();
    }
    public void LoadLevel()
    {
        punManager.LoadGame();
    }

    public bool IsNameApproved()
    {
        return this.NameApproved;
    }
    public bool IsErrorPerceivedApproved()
    {
        return this.ErrorPerceivedApproved;
    }
    public bool IsConnectionWithPhotonIssue()
    {
        return punManager.IsConnectionWithPhotonIssue;
    }
    public bool IsLeaveLobbyApproved()
    {
        return this.LeaveLobbyApproved;
    }
    public bool IsConnectedWithLobby()
    {
        return punManager.IsConnectedWithLobby;
    }
    public bool IsCreateRoomApproved()
    {
        return this.CreateRoomApproved;
    }
    public bool IsPlayerInRoom()
    {
        return punManager.IsPlayerInRoom;
    }
    public bool IsClientJoinedLeavedRoom()
    {
        return punManager.IsClientJoinedLeavedRoom;
    }
    public bool IsMasterClientSwitched()
    {
        return punManager.IsMasterClientSwitched;
    }
    public bool IsMaster()
    {
        return punManager.IsMaster();
    }
    public bool IsStartGameApproved()
    {
        return this.StartGameApproved;
    }
    public bool IsMasterStartedGame()
    {
        return punManager.IsMasterStartedGame;
    }
    public bool IsLeaveRoomApproved()
    {
        return this.LeaveRoomApproved;
    }
    public bool IsTimerReady()
    {
        return timer.IsReady();
    }

    public bool NameApproved
    {
        get { return nameApproved; }
        set { nameApproved = value; }
    }
    public bool ErrorPerceivedApproved
    {
        get { return errorPerceivedApproved; }
        set { errorPerceivedApproved = value; }
    }
    public bool LeaveLobbyApproved
    {
        get { return leaveLobbyApproved; }
        set { leaveLobbyApproved = value; }
    }
    public bool LeaveRoomApproved
    {
        get { return leaveRoomApproved; }
        set { leaveRoomApproved = value; }
    }
    public bool CreateRoomApproved
    {
        get { return createRoomApproved; }
        set { createRoomApproved = value; }
    }
    public bool StartGameApproved
    {
        get { return startGameApproved; }
        set { startGameApproved = value; }
    }

    public string Name
    {
        get { return name; }
        set { name = value; }
    }
    public Timer Timer
    {
        get { return timer; }
    }
    public WfPunManager PUNManager
    {
        get { return punManager; }
    }

    public void SetStateStart()
    {
        state = stateStart;
    }
    public void SetStateName()
    {
        state = stateName;
    }
    public void SetStateConnecting()
    {
        state = stateConnecting;
    }
    public void SetStateError()
    {
        state = stateError;
    }
    public void SetStateDisconnecting()
    {
        state = stateDisconnecting;
    }
    public void SetStateLobby()
    {
        state = stateLobby;
    }
    public void SetStateCreatingRoom()
    {
        state = stateCreatingRoom;
    }
    public void SetStateRoomAsClient()
    {
        state = stateRoomAsClient;
    }
    public void SetStateRoomAsMaster()
    {
        state = stateRoomAsMaster;
    }
    public void SetStateLeavingRoom()
    {
        state = stateLeavingRoom;
    }
    public void SetStateStartingGame()
    {
        state = stateStartingGame;
    }

    public IWfState StateStart
    {
        get { return (IWfState)stateStart; }
    }
    public IWfState StateName
    {
        get { return (IWfState)stateName; }
    }
    public IWfState StateConnecting
    {
        get { return (IWfState)stateConnecting; }
    }
    public IWfState StateError
    {
        get { return (IWfState)stateError; }
    }
    public IWfState StateLobby
    {
        get { return (IWfState)stateLobby; }
    }
    public IWfState StateDisconnecting
    {
        get { return (IWfState)stateDisconnecting; }
    }
    public IWfState StateCreatingRoom
    {
        get { return (IWfState)stateCreatingRoom; }
    }
    public IWfState StateRoomAsMaster
    {
        get { return (IWfState)stateRoomAsMaster; }
    }
    public IWfState StateRoomAsClient
    {
        get { return (IWfState)stateRoomAsClient; }
    }
    public IWfState StateLeavingRoom
    {
        get { return (IWfState)stateLeavingRoom; }
    }
    public IWfState StateStartingGame
    {
        get { return (IWfState)stateStartingGame; }
    }
}
