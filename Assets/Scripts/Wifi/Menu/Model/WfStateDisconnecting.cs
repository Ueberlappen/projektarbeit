﻿using UnityEngine;
using System.Collections;

public class WfStateDisconnecting : IWfState {

    public WfStateDisconnecting(WfConnectToPhotonModel model) : base(model)
    {

    }

    public override void NextStep()
    {
        if (isEntryBlocked == false)
        {
            entry();
        }

        if ((connectToPhotonModel.IsConnectedWithLobby() == false))
        {
            unblockEntry();
            connectToPhotonModel.SetStateName();
        }

        if (connectToPhotonModel.IsConnectionWithPhotonIssue() == true)
        {
            unblockEntry();
            connectToPhotonModel.SetStateError();
        }
    }

    private void entry()
    {
        SetChanged();
        notifyObservers("Disconnecting...");
        isEntryBlocked = true;
    }
    private void unblockEntry()
    {
        isEntryBlocked = false;
    }
}
