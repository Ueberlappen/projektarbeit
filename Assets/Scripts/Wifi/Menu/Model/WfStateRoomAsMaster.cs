﻿using UnityEngine;
using System.Collections;

public class WfStateRoomAsMaster : IWfState {

    public WfStateRoomAsMaster(WfConnectToPhotonModel model) : base(model)
    {

    }

    public override void NextStep()
    {
        if (isEntryBlocked == false)
        {
            entry();
        }
        
        if (connectToPhotonModel.IsLeaveRoomApproved() == true)
        {
            connectToPhotonModel.LeaveRoom();

            unblockEntry();
            connectToPhotonModel.SetStateLeavingRoom();
            connectToPhotonModel.LeaveRoomApproved = false;
        }

        if (connectToPhotonModel.IsStartGameApproved() == true)
        {
            connectToPhotonModel.CloseRoom();
            connectToPhotonModel.InformClients();
            connectToPhotonModel.StartTimer(3);

            unblockEntry();
            connectToPhotonModel.SetStateStartingGame();  
        }

        if (connectToPhotonModel.IsConnectionWithPhotonIssue() == true)
        {
            unblockEntry();
            connectToPhotonModel.SetStateError();
        }

        connectToPhotonModel.UpdatePlayersInRoom();
    }

    private void entry()
    {
        SetChanged();
        NotifyObservers();
        isEntryBlocked = true;
    }

    private void unblockEntry()
    {
        isEntryBlocked = false;
    }
}
