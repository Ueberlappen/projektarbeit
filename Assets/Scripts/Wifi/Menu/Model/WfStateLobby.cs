﻿using UnityEngine;
using System.Collections;

public class WfStateLobby : IWfState {

    public WfStateLobby(WfConnectToPhotonModel model) : base(model)
    {

    }

    public override void NextStep()
    {
        if (isEntryBlocked == false)
        {
            entry();
        }

        if (connectToPhotonModel.IsLeaveLobbyApproved() == true)
        {
            connectToPhotonModel.DisconnectFromPhoton();

            unblockEntry();
            connectToPhotonModel.SetStateDisconnecting();
            connectToPhotonModel.LeaveLobbyApproved = false;
        }

        if (connectToPhotonModel.IsCreateRoomApproved() == true)
        {
            connectToPhotonModel.CreateRoom();

            unblockEntry();
            connectToPhotonModel.SetStateCreatingRoom();
            connectToPhotonModel.CreateRoomApproved = false;
        }

        if(connectToPhotonModel.IsPlayerInRoom() == true)
        {
            unblockEntry();
            connectToPhotonModel.SetStateRoomAsClient();
        }

        if (connectToPhotonModel.IsConnectionWithPhotonIssue() == true)
        {
            unblockEntry();
            connectToPhotonModel.SetStateError();
        }

        connectToPhotonModel.UpdateOpenRooms();
    }

    private void entry()
    {
        SetChanged();
        NotifyObservers();
        isEntryBlocked = true;
    }
    private void unblockEntry()
    {
        isEntryBlocked = false;
    }
}
