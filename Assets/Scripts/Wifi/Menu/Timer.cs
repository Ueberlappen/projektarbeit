﻿using UnityEngine;
using System.Collections;
using System;

public class Timer : Observable
{
    private int startCounter = 0;
    private int currentCounter = -1; // Sont gleich Timer Ready da erst auf 0 abgefragt wird bevor dekrementiert wrid
    private float help = 0;

    public void SetStart(int time)
    {
        startCounter = time;
    }
    public bool IsReady()
    {
        if(currentCounter == 0)
        {
            return true;
        }
        else
        {
            return false;
        }
        
    }
    public void Decrement()
    {
        help += Time.deltaTime;
        currentCounter = startCounter - Mathf.FloorToInt(help);
        Debug.Log("Decrement " + currentCounter.ToString());
        SetChanged();
        notifyObservers(string.Format("Match starts in {0}...", currentCounter.ToString()));
    }
    public int CurrentCounter
    {
        get { return currentCounter; }
        set { currentCounter = value; }
    }
}
