﻿using UnityEngine;
using System.Collections;

public interface Observer{

    void update(IObservable o, string message);
}
