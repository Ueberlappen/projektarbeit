﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.SceneManagement;

public class WfConnectToPhotonController : MonoBehaviour {

    public WfConnectToPhotonView connectToPhotonView;
    private WfConnectToPhotonModel connectToPhotonModel;
    public WfConnectToPhotonAdapter connectToPhotonAdapter;

	void Start ()
    {
        connectToPhotonModel = new WfConnectToPhotonModel(this.GetComponent<WfPunManager>());

        connectToPhotonAdapter = new WfConnectToPhotonAdapter(connectToPhotonView, connectToPhotonModel);
        connectToPhotonAdapter.RegisterEvents();
	}
	void Update ()
    {

        connectToPhotonModel.NextStep();
	}

    public void OnClickBtnErrorPerceived()
    {
        connectToPhotonModel.ErrorPerceivedApproved = true;
    }
    public void OnEndEditInFdPlayerName()
    {
        connectToPhotonModel.Name = connectToPhotonView.inFdPlayerName.text;
        connectToPhotonModel.NameApproved = true;
    }
    public void OnClickBtnBack()
    {
        SceneManager.LoadScene("SceneMainMenu");
    }

    public void OnClickBtnCreateRoom()
    {
        connectToPhotonModel.CreateRoomApproved = true;
    }
    public void OnClickBtnLeaveLobby()
    {
        connectToPhotonModel.LeaveLobbyApproved = true;
    }
    
    public void OnClickBtnStartGame()
    {
        connectToPhotonModel.StartGameApproved = true;
    }
    public void OnClickBtnLeaveRoom()
    {
        connectToPhotonModel.LeaveRoomApproved = true;
    }
}
