﻿using UnityEngine;
using System.Collections;
using TrueSync;

class WfPlayer : TrueSyncBehaviour {

	WfMovementPlayerHuman moveManager;
	public GameObject Cam_ref;
    /**
    * @brief Key to set/get horizontal position from {@link TrueSyncInput}.
    **/
    private const byte INPUT_KEY_HORIZONTAL = 0;

    /**
    * @brief Key to set/get vertical position from {@link TrueSyncInput}.
    **/
    private const byte INPUT_KEY_VERTICAL = 1;

    private TSRigidBody2D _TtsRigidBody;

    /**
    * @brief Initial setup when game is started.
    **/
    public override void OnSyncedStart()
    {
        //.position = new TrueSync.TSVector(-3 + (owner.Id - 1) * 4, 1, 0);
        Debug.Log("OnSyncedINput");
        _TtsRigidBody = gameObject.GetComponent<TSRigidBody2D>();
		moveManager = gameObject.GetComponent<WfMovementPlayerHuman> ();
		Cam_ref = GameObject.Find("GCamMain");
		Cam_ref.GetComponent<CameraFollow>().set_playerCamera(this); // MainCam konzentriert sich auf spieler1
		//Cam_ref.GetComponent<CameraFollow>().set_playerCamera(this);

		/* TODO: Mit playerID von truesync vergleichen. Wenn ID durcheinander: Vielleicht hilft SocketDecoder wie bei BA
		 * Jeder braucht ne Kamera sonst gibts probleme
		 * mit move to position unschönes gefühl
		 * bounce für tsmaterial suchen
		switch (playerNr)
		{
		case 1:                
			Cam_ref.GetComponent<CameraFollow>().set_playerCamera(this); // MainCam konzentriert sich auf spieler1
			Goal_ref.transform.position = new Vector3(-1.8f, -59.1f, 0);//erstelle in den nächsten Zeilen Codes für alle spieler ein Tor an der richtigen Position
			Goal_ref.transform.rotation = Quaternion.Euler(0f, 0f, 181.6f);
			Goal_ref.transform.localScale = new Vector3(24.711f, 22.29f, 1);
			break;
		case 2:
			LP_ref= GameObject.Find("TxPl2LP").GetComponent<Text>();
			Goal_ref.transform.position = new Vector3(-57.8f, -0.8f, 0);
			Goal_ref.transform.rotation = Quaternion.Euler(0f, 0f, 89.15f);
			Goal_ref.transform.localScale = new Vector3(22.925f, 23.468f, 1f);

			break;
		case 3:
			LP_ref = GameObject.Find("TxPl3LP").GetComponent<Text>();
			Goal_ref.transform.position = new Vector3(0f, 52.4f, 0);
			Goal_ref.transform.rotation = Quaternion.Euler(0f, 0f, -0.3f);
			Goal_ref.transform.localScale = new Vector3(25.303f, 25.039f, 1);
			break;
		case 4:
			LP_ref = GameObject.Find("TxPl4LP").GetComponent<Text>();
			Goal_ref.transform.position = new Vector3(54.8f, -1.9f, 0);
			Goal_ref.transform.rotation = Quaternion.Euler(0f, 0f, -87.8f);
			Goal_ref.transform.localScale = new Vector3(21.326f, 23.883f, 1f);

			break;

		}*/
    }

    /**
    * @brief Sets player inputs.
    **/
    public override void OnSyncedInput()
    {
		Vector3 movement = moveManager.GetMovement ();
		float hor = movement.x;
		float ver = movement.y;

        TrueSyncInput.SetInt(INPUT_KEY_HORIZONTAL, (int)(hor)*100);
        TrueSyncInput.SetInt(INPUT_KEY_VERTICAL, (int)(ver)*100);

    }

    /**
    * @brief Updates ball's movements and instantiates new ball objects when player press space.
    **/
    public override void OnSyncedUpdate()
	{

		// Get horizontal and vertical movement -> funktion
		FP hor = (FP)TrueSyncInput.GetInt (INPUT_KEY_HORIZONTAL) / 100;
		FP ver = (FP)TrueSyncInput.GetInt (INPUT_KEY_VERTICAL) / 100;

		// Add force -> funtktion
		TSVector2 forceToApply = TSVector2.zero;
		forceToApply.x = hor;

		forceToApply.y = ver;

		_TtsRigidBody.MovePosition (forceToApply);
	}


    /**
         * @brief Called when the game has paused.
         **/
    public override void OnGamePaused() { }

    /**
     * @brief Called when the game has unpaused.
     **/
    public override void OnGameUnPaused() { }

    /**
     * @brief Called when the game has ended.
     **/
    public override void OnGameEnded() { }

    /**
     *  @brief Called before {@link #OnSyncedUpdate}.
     *  
     *  Called once every lockstepped frame.
     */
    public override void OnPreSyncedUpdate() { }

    /**
     * @brief Callback called when a player get disconnected.
     **/
    public override void OnPlayerDisconnection(int playerId) {
        TrueSyncManager.RemovePlayer(playerId);
    }
}
