﻿using UnityEngine;
using System.Collections;
using TrueSync;

public class WifiGameModel : OfflineGameModel {

    //private GameObject trueSyncManager;
    private TrueSyncManager trueSyncManager;

    public WifiGameModel(TrueSyncManager tsm)
    {
		playerInstance = (GameObject)Resources.Load("GWfPLayerHuman");//Settingsladen
        trueSyncManager = tsm;
        

		ballInstance = (GameObject)Resources.Load("GWfBall");

        //trueSyncManager = (GameObject)Resources.Load("GWfTrueSyncManager");
        //trueSyncManager.GetComponent<TrueSyncManager>().playerPrefabs[0] = playerInstance;
    }

    public override void InstantiatePlayer()
    {
		trueSyncManager.GetComponent<TrueSyncManager>().playerPrefabs[0] = playerInstance;
    }

    public override void LeaveGame()
    {
        WfConnectToPhotonModel.punManager.Disconnect();
    }
}
