﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MainMenuView : MonoBehaviour{

    [Header("Main Menu")]
    public GameObject pnMainMenu;
    public Button btnMainMenuSinglePlayer;
    public Button btnMainMenuMultiPlayer;
    public Button btnMainMenuOptions;
    public Button btnMainMenuExit;

    [Header("Singleplayer Main")]
    public GameObject pnSingleplayerMain;
    public Button btnSingleplayerMainStart;
    public Button btnSingleplayerMainBack;

    [Header("Multiplayer Main")]
    public GameObject pnMultiplayerMain;
    public Button btnBtMultiplayerMain;
    public Button btnWfMultiplayerMain;
    public Button btnMultiplayerMainBack;

    [Header("Options Main")]
    public GameObject pnOptionsMain;
    public Slider sldOptionsMainMasterVol;
    public Slider sldOptionsMainMusicVol;
    public Slider sldOptionsMainSFXVol;
    public Button btnOptionsMainBack;
}
