﻿using UnityEngine;
using System.Collections;

public class MainMenuAdapter : Observer
{
    private MainMenuView mainMenuView;
    private MainMenuModel mainMenuModel;

    public MainMenuAdapter(MainMenuView view, MainMenuModel model)
    {
        this.mainMenuView = view;
        this.mainMenuModel = model;
    }

    public void registerEvents()
    {
        mainMenuModel.AddObserver(this);
    }
    public void update(IObservable o, string message)
    {
    }
}
