﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainMenuController : MonoBehaviour {

    public MainMenuView mainMenuView;
    private MainMenuModel mainMenuModel;

    void Start()
    {
        Screen.sleepTimeout = SleepTimeout.NeverSleep;
        mainMenuView.pnMainMenu.SetActive(true);
    }

    public void OnClickBtnMainMenuSinglePlayer()
    {
        mainMenuView.pnMainMenu.SetActive(false);
        mainMenuView.pnSingleplayerMain.SetActive(true);
        
    }

    public void OnClickBtnMainMenuMultiplayer()
    {
        mainMenuView.pnMainMenu.SetActive(false);
        mainMenuView.pnMultiplayerMain.SetActive(true);
        
    }

    public void OnClickBtnMainMenuOptions()
    {
        mainMenuView.pnMainMenu.SetActive(false);
        mainMenuView.pnOptionsMain.SetActive(true);
    }

    public void OnClickBtnMainMenuExit()
    {
        Application.Quit();
    }



    public void OnClickBtnSingleplayerMainStart()
    {
        GameModeSettings.gameMode = EGameMode.GameModeOffline;
        SceneManager.LoadScene("SceneGame");
    }



    public void OnClickBtnBtMultiplayerMain()
    {
        SceneManager.LoadScene("SceneBtMenu");
    }

    public void OnClickBtnWfMultiplayerMain()
    {
        SceneManager.LoadScene("SceneWfMenu");
    }



    public void OnClickBtnMainBack()
    {
        mainMenuView.pnSingleplayerMain.SetActive(false);
        mainMenuView.pnMultiplayerMain.SetActive(false);
        mainMenuView.pnOptionsMain.SetActive(false);
        mainMenuView.pnMainMenu.SetActive(true);
    }
}
