﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class MainMenuModel : Observable{

    private SceneManager sceneManager;

    public MainMenuModel(){

        sceneManager = new SceneManager();
    }

    public SceneManager GetSceneManger()
    {
        return sceneManager;
    }
}
