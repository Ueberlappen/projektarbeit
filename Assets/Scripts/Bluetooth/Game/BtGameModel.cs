﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class BtGameModel : OfflineGameModel
{
    private BtNetworkManager btNetworkManager;
	
    public BtGameModel ()
    {
        btNetworkManager = BtNetworkManager.Instance;
        Debug.Log ("BtGameModel: BtGameModel() \"GameModel create Player and Ball Objects\"END\n");
    }
	
    /****************************************************
    *   -  Add Player to ClientScene                    *
    ****************************************************/
    public override void InstantiatePlayer ()
    {
        Debug.Log ("BtGameModel: InstantiatePlayer() END\n");
        ClientScene.AddPlayer (0);
    }

    /****************************************************
    *   -  Add Ball to ClientScene                      *
    ****************************************************/
    public override void InstantiateBall ()	
    {
        //Controller Inits Ball when Game Starts
        //Ball can be Created with "NetworkManager.singleton.client.Send(BtMsgInitBall.kMessageType, new BtMsgInitBall{});"
    }
}
