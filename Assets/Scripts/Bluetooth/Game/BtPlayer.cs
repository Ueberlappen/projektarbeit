﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

class BtPlayer : OfflinePlayer
{

    private Rigidbody2D btRigidBody;
	private CircleCollider2D btCircleCollider;
	public GameObject ballInstance;
	public static bool initBall = false;
	public static bool initGoal = false;
	public static bool resetBallPos = false;
	public static int btPlayerID; 

	void Start()
	{
        if(isLocalPlayer){
            Cam_ref = GameObject.Find("GCamMain");
            HUD_ref = GameObject.Find("GCnvGameView");
            Cam_ref.GetComponent<CameraFollow>().set_playerCamera(this);
        }
	}

	void Update()
	{
		if (initBall) {
			CmdSpawnBall ();
			initBall = false;
		}
		if(initGoal)
		{
            Debug.Log ("UnityBluetooth: \tBtPlayer:\t Update - initGoal == true \n");
            GetGoalReference();
            initGoal = false;
		}
		if(resetBallPos)
		{
            Debug.Log ("UnityBluetooth: \tBtPlayer:\t Update - NewBallPos \n");
            NewBallPos();
            resetBallPos = false;
		}
	}
	
    static int i = 0;
    public override void FixedUpdate()
    {
        if(isLocalPlayer)
        {
            if(i == 500)
            {
                Debug.Log ("UnityBluetooth: \tBtPlayer:\t FixedUpdate - Lifepoints = "+lifepoints+"\n");
                i = 0;
            }
            else{i++;}
            // Lifepoints durch Herzen representieren
            HUD_ref.GetComponent<HUD>().HeartUI.sprite = HUD_ref.GetComponent<HUD>().HeartSprites[lifepoints]; 
            if (lifepoints <= 0){
                Debug.Log ("UnityBluetooth: \tBtPlayer:\t FixedUpdate - DIE!!! \n");
                Goal_ref.GetComponent<SpriteRenderer>().color = Color.black;
                Goal_ref.GetComponent<PolygonCollider2D>().isTrigger = false;
                Destroy(gameObject);
            }
        }
    }
    
    public static void SpawnBall()
	{
        Debug.Log ("UnityBluetooth: \tBtPlayer:\t SpawnBall - wird aufgerufen\n");
        initBall = true;
	}
	
    // This [Command] code is called on the Client …
    // … but it is run on the Server!
    [Command]
    public void CmdSpawnBall()
	{
        Debug.Log ("UnityBluetooth: \tBtPlayer:\tServer: CmdSpawnBall - getReference PlayerID: "+btPlayerID+" \n");
		ballInstance = (GameObject)GameObject.Instantiate (ballInstance, new Vector2 (Random.Range (-10.0f, 10.0f), Random.Range (-10.0f, 10.0f)), new Quaternion (0, 0, 0, 0));
		NetworkServer.Spawn (ballInstance);
	}
	
	public static void ResetBallPos()
	{
        Debug.Log ("UnityBluetooth: \tBtPlayer:\t ResetBallPos() - wird ausgeführt\n");
        resetBallPos = true;
	}
	
    public void NewBallPos()
	{
        Debug.Log ("UnityBluetooth: \tBtPlayer:\t NewBallPos (alt):"+ballInstance.GetComponent<Transform>().position+"\n");
        ballInstance.GetComponent<Transform>().position = new Vector2(0,0);
        Debug.Log ("UnityBluetooth: \tBtPlayer:\t NewBallPos(neu):"+ballInstance.GetComponent<Transform>().position+"\n");
	}
	
	public static void GetGoal(int playerID)
	{
        Debug.Log ("UnityBluetooth: \tBtPlayer:\t SpawnGoal - wird aufgerufen \n");
        Debug.Log ("UnityBluetooth: \tBtPlayer:\t SpawnGoal - PlayerID = "+playerID);
        
        btPlayerID = playerID; 
        initGoal = true;
	}
	
	private void GetGoalReference()
	{
        switch (btPlayerID)
        {
            case 1:   
                Debug.Log ("UnityBluetooth: \tBtPlayer:\t GetGoalReference - getReference PlayerID: "+btPlayerID+" \n");
                Goal_ref = GameObject.Find("GGoal1");
                Debug.Log ("UnityBluetooth: \tBtPlayer:\t GetGoalReference - Pos:"+Goal_ref.GetComponent<Transform>().position+"\n");
                Goal_ref.GetComponent<Goal>().SetMyGoalColor();
                Goal_ref.GetComponent<Goal>().Set_player(this);
                break;
            case 2:
                Debug.Log ("UnityBluetooth: \tBtPlayer:\t GetGoalReference - getReference PlayerID: "+btPlayerID+" \n");
                Goal_ref = GameObject.Find("GGoal2");
                Goal_ref.GetComponent<Goal>().SetMyGoalColor();
                Goal_ref.GetComponent<Goal>().Set_player(this);
                break;
            case 3:
                Debug.Log ("UnityBluetooth: \tBtPlayer:\t GetGoalReference - getReference PlayerID: "+btPlayerID+" \n");
                Goal_ref = GameObject.Find("GGoal3");
                Goal_ref.GetComponent<Goal>().SetMyGoalColor();
                Goal_ref.GetComponent<Goal>().Set_player(this);
                break;
            case 4:
                Debug.Log ("UnityBluetooth: \tBtPlayer:\t GetGoalReference - getReference PlayerID: "+btPlayerID+" \n");
                Goal_ref = GameObject.Find("GGoal4");
                Goal_ref.GetComponent<Goal>().SetMyGoalColor();
                Goal_ref.GetComponent<Goal>().Set_player(this);
                break;
        }
	}
	
	[Command]
    public void CmdUpdateLifePoints()
	{
        Debug.Log ("UnityBluetooth: \tBtPlayer:\tServer: CmdUpdateLifePoints - getReference PlayerID: "+btPlayerID+" \n");
        if (lifepoints > 0)
        {
            lifepoints -= 1;
        }
	}
	
	public override void Tor_kassiert()
    {
        CmdUpdateLifePoints();
        CmdSpawnBall();
        Debug.Log ("UnityBluetooth: \tBtPlayer:\t Tor_kassiert - Lifepoints = "+lifepoints+"\n");
        NetworkManager.singleton.client.Send(BtMsgResetBallPos.kMessageType, new BtMsgResetBallPos{});
    }
}
