﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using LostPolygon.AndroidBluetoothMultiplayer;

public class BtMenuView : MonoBehaviour {

	[Header("Bluetooth Menu")]
	public GameObject pnBtBluetoothMenu;
	public Button btnBtBluetoothMenuBack;
	public Button btnBtBluetoothMenuHost;
	public Button btnBtBluetoothMenuClient;

	[Header("Host Menu")]
	public GameObject pnBtHostMenu;
	public Button btnBtHostMenuBack;
	public Button btnBtHostMenuStart;

	[Header("Client Menu")]
	public GameObject pnBtClientMenu;
	public Button btnBtClientMenuBack;

	[Header("Player List")]
	public GameObject pnBtPlayerList;
	public Text txtBtPlayerList;
	public Text txtBtConnectMode;
}
