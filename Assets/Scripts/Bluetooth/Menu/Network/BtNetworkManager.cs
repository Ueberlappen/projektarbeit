﻿using UnityEngine;
using UnityEngine.Networking;
using Random = UnityEngine.Random;
using LostPolygon.AndroidBluetoothMultiplayer;
using UnityEngine.SceneManagement;
using System.Collections;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

using UnityEngine.UI;
using System.Text;


public class BtNetworkManager : AndroidBluetoothNetworkManager
{

    private List<string> playerList;
    
    [Header("Bluetooth Controller")]
    public BtMenuController btController;

/*******************************************************************************************************************
*                                                                                                                  *
*                               Initialitation RegisterHandler for NetworkMessages                                 *
*                                                                                                                  *
*******************************************************************************************************************/
    /****************************************************
    *   -  IsCalled when Server or/and Host is Created  *
    *   -  Register all Message which a Client can      *
    *       send to the Server                          *
    ****************************************************/
    public override void OnStartServer (){
        Debug.Log ("BtNetworkManager: OnStartServer() \"RegisterHandler\" END\n");
        base.OnStartServer ();
        playerList = new List<string>();
        // Register the handler for the NetworkMessages that is sent from client to server
        NetworkServer.RegisterHandler (BtMsgGameStart.kMessageType, OnServerGameStart);
        NetworkServer.RegisterHandler (BtMsgInitBall.kMessageType, OnServerInitBall);
        NetworkServer.RegisterHandler (BtMsgMyDevice.kMessageType, OnServerMyDevice);
        NetworkServer.RegisterHandler (BtMsgUpdateDeviceList.kMessageType, OnServerUpdateDeviceList);
        NetworkServer.RegisterHandler (BtMsgGetGoal.kMessageType, OnServerGetGoals);
        NetworkServer.RegisterHandler (BtMsgResetBallPos.kMessageType, OnServerResetBallPos);
        
	}

 
    /****************************************************
    *   -  Is called on Server when a Client is Ready   *
    ****************************************************/
    public override void OnServerReady (NetworkConnection conn)
    {
        Debug.Log ("UnityBluetooth: \tNetworkManager:\tServer: OnServerReady - wird aufgerufen\n");
        Debug.Log ("BtNetworkManager: OnServerReady():\n");
        Debug.Log("BtNetworkManager: \"Address:\"" + conn.address);
        Debug.Log("BtNetworkManager: \"HostID:\"" + conn.hostId);
        Debug.Log("BtNetworkManager: \"ConnectionID:\"" + conn.connectionId);
        Debug.Log("BtNetworkManager: \"GetCurrentDevice():\"" + GetCurrentDevice());
        Debug.Log("BtNetworkManager: \"GetCurrentMode():\"" + GetCurrentMode());
        
        base.OnServerReady (conn);
        NetworkManager.singleton.client.Send(BtMsgUpdateDeviceList.kMessageType, new BtMsgUpdateDeviceList{});
    }
    
    
    /****************************************************
    *   -Is called on Server when a Client Disconnects  *
    ****************************************************/
    public override void OnServerDisconnect(NetworkConnection conn)
    {
        Debug.Log ("BtNetworkManager: OnServerDisconnect(): Client verabschiedet sich\n");
        base.OnServerDisconnect (conn);
        NetworkManager.singleton.client.Send(BtMsgUpdateDeviceList.kMessageType, new BtMsgUpdateDeviceList{});
    }
	
   /****************************************************
    *   -  IsCalled when Client or/and Host is Created  *
    *   -  Register all Message which the Server can    *
    *       send to a Client                            *
    ****************************************************/
    public override void OnStartClient (NetworkClient client)
    {
    
        Debug.Log ("UnityBluetooth: \tNetworkManager:\tClient: OnStartClient - wird aufgerufen\n");
        base.OnStartClient (client);
        // Register the handler for the NetworkMessages that is sent from server to clients
        client.RegisterHandler (BtMsgGameStart.kMessageType, OnClientGameStart);
        client.RegisterHandler (BtMsgInitBall.kMessageType, OnClientInitBall);
        client.RegisterHandler (BtMsgMyDevice.kMessageType, OnClientMyDevice);
        client.RegisterHandler (BtMsgUpdateDeviceList.kMessageType, OnClientUpdateDeviceList);
        client.RegisterHandler (BtMsgGetGoal.kMessageType, OnClientGetGoals);
        client.RegisterHandler (BtMsgResetBallPos.kMessageType, OnClientResetBallPos);
	}
	/****************************************************
    * IsCalled when Client when Host/Client Disconnects *
    *                                                   *
    ****************************************************/
    public override void OnStopClient ()
    {
        base.OnStopClient();
        if(SceneManager.GetActiveScene().name == "SceneBtMenu"){
            btController.ClientDisconnect();
        }
        else{
            SceneManager.LoadScene("SceneMainMenu");
            DestroyBtNetworkManager();
        }
    }
/*******************************************************************************************************************
*                                                                                                                  *
*                                      Handle NetworkMessage "GameStart"                                           *
*                                                                                                                  *
*******************************************************************************************************************/
    /*Fnct is called from the Host if he wants to start the game*/
    private void OnServerGameStart (NetworkMessage netMsg){
        Debug.Log ("BtNetworkManager: OnServerGameStart(NetworkMessage) \"Send Message Client->Server\" END\n");
        BtMsgGameStart btStartGame = netMsg.ReadMessage<BtMsgGameStart> ();
	/*Call on every Client "GameStart"*/
        foreach (NetworkConnection connection in NetworkServer.connections) {
            if (connection == null)
            {
                Debug.Log ("BtNetworkManager: MessageHandler \"connection == null\" END\n");
                continue;
            }
            Debug.Log ("BtNetworkManager: MessageHandler \"connection.Send()\" END\n");
            connection.Send (BtMsgGameStart.kMessageType, btStartGame);
        }
    }
    
    /*SetGameMode to Bluetooth and Load GameScene for Client*/
    private void OnClientGameStart (NetworkMessage netMsg){
        Debug.Log ("BtNetworkManager: OnClientGameStart(NetworkMessage) \"Send Message Server->Client\" END\n");
        GameModeSettings.gameMode = EGameMode.GameModeBluetooth;
        SceneManager.LoadScene ("SceneGame");
    }

/*******************************************************************************************************************
*                                                                                                                  *
*                                      Handle NetworkMassage "InitGoals"                                           *
*                                                                                                                  *
*******************************************************************************************************************/

    private void OnServerGetGoals(NetworkMessage netMsg){
        Debug.Log ("UnityBluetooth: \tNetworkManager:\tServer: OnServerSpawnGoals - wird aufgerufen\n");
        BtMsgGetGoal btGetGoal = netMsg.ReadMessage<BtMsgGetGoal> ();
        /*Call on every Client "GameStart"*/
        foreach (NetworkConnection connection in NetworkServer.connections) {
            if (connection == null)
            {
                continue;
            }
            Debug.Log ("UnityBluetooth: \tNetworkManager:\tServer: OnServerGetGoals - connection.Send() wird aufgerufen\n");
            connection.Send (BtMsgGetGoal.kMessageType, btGetGoal);
            btGetGoal.playerID += 1;
        }
    }
    
    private void OnClientGetGoals(NetworkMessage netMsg){
        Debug.Log ("UnityBluetooth: \tNetworkManager:\tClient: OnClientGetGoals - wird aufgerufen\n");
        BtMsgGetGoal btGetGoal = netMsg.ReadMessage<BtMsgGetGoal> ();
        BtPlayer.GetGoal(btGetGoal.playerID);
    
    }

/*******************************************************************************************************************
*                                                                                                                  *
*                                      Handle NetworkMessage "InitBall"                                            *
*                                                                                                                  *
*******************************************************************************************************************/
    private void OnServerInitBall (NetworkMessage netMsg){
        Debug.Log("BtNetworkManager: OnServerInitBall() \n");
        BtMsgInitBall btInitBall = netMsg.ReadMessage<BtMsgInitBall>();
        
        foreach(NetworkConnection connection in NetworkServer.connections)
        {
            if(connection.hostId == -1)
            {
                Debug.Log("BtNetworkManager: OnServerInitBall() \"Address:\"" + connection.address);
                Debug.Log("BtNetworkManager: OnServerInitBall() \"if(Host) -> InitBall()\"\n");
                connection.Send(BtMsgInitBall.kMessageType, btInitBall);
            }
        }
    }
    
    private void OnClientInitBall (NetworkMessage netMsg){
        Debug.Log ("BtNetworkManager: OnClientInitBall() \"if(Host) -> InitBall()\" END\n");
        BtPlayer.SpawnBall();
    }

/*******************************************************************************************************************
*                                                                                                                  *
*                                      Handle NetworkMessage "ResetBallPos"                                            *
*                                                                                                                  *
*******************************************************************************************************************/
    private void OnServerResetBallPos(NetworkMessage netMsg){
        BtMsgResetBallPos btResetBall = netMsg.ReadMessage<BtMsgResetBallPos>();
        Debug.Log ("UnityBluetooth: \tNetworkManager:\tServer ResetBallPos(): - wird ausgeführt\n");
        foreach(NetworkConnection connection in NetworkServer.connections)
        {
            if(connection.hostId == -1)
            {
                Debug.Log ("UnityBluetooth: \tNetworkManager:\tServer ResetBallPos(): - Host wird aufgerufen\n");
                connection.Send(BtMsgResetBallPos.kMessageType, btResetBall);
            }
        }
    }
    
    private void OnClientResetBallPos(NetworkMessage netMsg)
    {
        Debug.Log ("UnityBluetooth: \tNetworkManager:\tClient ResetBallPos(): - Host wird ausgeführt\n");
        BtPlayer.ResetBallPos();
        
    }
    
    
/*******************************************************************************************************************
*                                                                                                                  *
*                                      Handle NetworkMessage "MyDevice"                                            *
*                                                                                                                  *
*******************************************************************************************************************/
    private void OnServerUpdateDeviceList(NetworkMessage netMsg){
        playerList.Clear();
        Debug.Log("BtNetworkManager: OnServerUpdateDeviceList() \n");
        foreach(NetworkConnection connection in NetworkServer.connections)
        {
            if (connection == null)
            {
                Debug.Log ("BtNetworkManager: OnServerUpdateDeviceList \"connection == null\" END\n");
                continue;
            }
            connection.Send(BtMsgMyDevice.kMessageType, new BtMsgMyDevice{});
        }
    }
  
    private void OnServerMyDevice(NetworkMessage netMsg){   
        BtMsgMyDevice btMyDevice = netMsg.ReadMessage<BtMsgMyDevice>();
        playerList.Add(btMyDevice.connectionMode + "\t\t" + btMyDevice.deviceName);
        
        string list = "";
        foreach(string player in playerList)
        {
            Debug.Log("BtNetworkManager: OnServerMyDevice(): Add Player to String");
            list += (player + "\n\n");
        }
        foreach(NetworkConnection connection in NetworkServer.connections)
        {
            if (connection == null)
            {
                Debug.Log ("BtNetworkManager: OnServerMyDevice \"connection == null\" END\n");
                continue;
            }
            connection.Send(BtMsgUpdateDeviceList.kMessageType, new BtMsgUpdateDeviceList{deviceList = list});
        }
        Debug.Log("BtNetworkManager: OnServerMyDevice(): Updated");
    }
    
    private void OnClientMyDevice(NetworkMessage netMsg){
        Debug.Log("BtNetworkManager: OnClientMyDevice() \n");
        NetworkManager.singleton.client.Send(BtMsgMyDevice.kMessageType, new BtMsgMyDevice{deviceName = GetCurrentDevice(),
        connectionMode = GetCurrentMode()});
    }
    
    private void OnClientUpdateDeviceList(NetworkMessage netMsg){    
        BtMsgUpdateDeviceList btUpdateDeviceList = netMsg.ReadMessage<BtMsgUpdateDeviceList>();
        btController.UpdatePlayerList(btUpdateDeviceList.deviceList);
    }
/*******************************************************************************************************************
*                                                                                                                  *
*                                           Dont Destroy on Load                                                   *
*                                                                                                                  *
*******************************************************************************************************************/
    //Deklarate a new Instance of BtNetworkManager
    private static BtNetworkManager instance = null;

    public static BtNetworkManager Instance {
            get { return instance; }
    }

    public AndroidBluetoothNetworkManagerHelper GameObjectInstance { 
      get { return this.gameObject.GetComponent<AndroidBluetoothNetworkManagerHelper> (); }
    }


    //if BtNetworkManager already exists getIntance 
    void Awake ()
    {
        if (instance != null && instance != this) {
            Destroy (this.gameObject);
            return;
        } else {
            instance = this;
        }
        DontDestroyOnLoad (this.gameObject);
    }
    
    public void DestroyBtNetworkManager()
    {
         Destroy (this.gameObject);
    }
/*******************************************************************************************************************
*                                                                                                                  *
*                                           Device Infos                                                           *
*                                                                                                                  *
*******************************************************************************************************************/   
        private string GetCurrentDevice()
	{
            BluetoothDevice device = AndroidBluetoothMultiplayer.GetCurrentDevice();
            return device.Name;
	}

	private string GetCurrentMode()
	{
		return ("" + AndroidBluetoothMultiplayer.GetCurrentMode ());
	}
}
