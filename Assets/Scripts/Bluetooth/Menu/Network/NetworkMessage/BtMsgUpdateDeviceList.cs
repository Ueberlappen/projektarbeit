﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;


public class BtMsgUpdateDeviceList : MessageBase {

	public const short kMessageType = 1154;
	
	public string deviceList;
	//List können nicht mit MessageBase verschickt werden
	//public List<string> list;
}
