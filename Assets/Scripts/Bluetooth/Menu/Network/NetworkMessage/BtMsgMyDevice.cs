﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class BtMsgMyDevice : MessageBase {

	public const short kMessageType = 1155;
	
	public string deviceName;
	public string connectionMode;

	//public bool isHostReady;
}