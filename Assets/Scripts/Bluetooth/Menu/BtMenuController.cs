﻿using UnityEngine;
using System.Collections;
using LostPolygon.AndroidBluetoothMultiplayer;
using LostPolygon;
using System;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using System.Collections.Generic;


/********************************************************************************
*                                                                               *
*                   Contains all OnClickFnct from the BtMenuPanel               *
*                                                                               *  
********************************************************************************/
public class BtMenuController : MonoBehaviour {

    public BtMenuView btMenuView;

    [Header("Bluetooth Networking")]
    public AndroidBluetoothNetworkManagerHelper btAndroidNetworkManagerHelper;
    public BtNetworkManager btNetworkManager;

    #if UNITY_ANDROID

    //Listener fuer Bluetooth Menu Panel
    public void OnClickBluetoothMenuHost(){
        btAndroidNetworkManagerHelper.StartHost (); 
        btMenuView.pnBtBluetoothMenu.SetActive (false);
        btMenuView.pnBtHostMenu.SetActive (true);
        btMenuView.pnBtPlayerList.SetActive (true);
        btMenuView.txtBtConnectMode.text = "Host";
    }
    public void OnClickBluetoothMenuClient(){
        btAndroidNetworkManagerHelper.StartClient (); 
        btMenuView.pnBtBluetoothMenu.SetActive (false);
        btMenuView.pnBtClientMenu.SetActive (true);
        btMenuView.pnBtPlayerList.SetActive (true);
        btMenuView.txtBtConnectMode.text = "Client";
    }
    public void OnClickBluetoothMenuQuit(){
		btNetworkManager.DestroyBtNetworkManager();//Application.Quit();
		SceneManager.LoadScene("SceneMainMenu");
    }

    //Listener fuer Host/Client Menu Panel
    public void OnClickHostMenuStart(){
        NetworkManager.singleton.client.Send(BtMsgGameStart.kMessageType, new BtMsgGameStart{});
        NetworkManager.singleton.client.Send(BtMsgGetGoal.kMessageType, new BtMsgGetGoal{});
        NetworkManager.singleton.client.Send(BtMsgInitBall.kMessageType, new BtMsgInitBall{});
    }
    public void OnClickHostMenuDisconnect()
    {
         try{
            AndroidBluetoothMultiplayer.StopDiscovery ();
            AndroidBluetoothMultiplayer.Stop ();         
            btNetworkManager.StopHost ();
            Debug.Log ("BtMenuController: OnClickHostClientMenuBack() \"Bluetoothverbindung beendet \" END\n");
        }
        catch(Exception e){
            Debug.Log ("BtMenuController: OnClickHostClientMenuBack() \"FEHLER: Bluetoothverbindung nicht beendet \" END\n");
            Application.Quit();
        }	
        btMenuView.txtBtPlayerList.text = "Not Connected";
        btMenuView.pnBtHostMenu.SetActive (false);
        btMenuView.pnBtPlayerList.SetActive (false);
        btMenuView.pnBtBluetoothMenu.SetActive (true);
    }
    public void OnClickClientMenuDisconnect(){
    
        try{
            AndroidBluetoothMultiplayer.StopDiscovery ();
            AndroidBluetoothMultiplayer.Stop ();         
            btNetworkManager.StopClient ();
            Debug.Log ("BtMenuController: OnClickHostClientMenuBack() \"Bluetoothverbindung beendet \" END\n");
        }
        catch(Exception e){
            Debug.Log ("BtMenuController: OnClickHostClientMenuBack() \"FEHLER: Bluetoothverbindung nicht beendet \" END\n");
            Application.Quit();
        }	
        ClientDisconnect();
    }
    
    public void UpdatePlayerList(string playerList)
    {
        btMenuView.txtBtPlayerList.text = playerList;
        Debug.Log("BtMenuController: PlayerList -> Updated");
    }
    
    public void ClientDisconnect()
    {
        btMenuView.txtBtPlayerList.text = "Not Connected";
        btMenuView.pnBtClientMenu.SetActive (false);
        btMenuView.pnBtPlayerList.SetActive (false);
        btMenuView.pnBtBluetoothMenu.SetActive (true);
    }
    
    
    
    #else


    #endif
}
