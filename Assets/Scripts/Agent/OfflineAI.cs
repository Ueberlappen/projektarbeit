﻿using UnityEngine;
using System.Collections;

public class OfflineAI : OfflinePlayer
{
	//AI
	public float maxSpeed;
	public float maxAccel;
	public float orientation;
	public float rotation;
	public Vector3 velocity;
	protected Controlling controlling;

	public override void Start ()
	{
		velocity = Vector3.zero;
		controlling = new Controlling ();
		maxSpeed = 800.0f;
		maxAccel = 400.0f;

		base.Start ();
	}

	public override void FixedUpdate ()
	{
		base.FixedUpdate ();


		Vector3 displacement = velocity * Time.deltaTime;
		orientation += rotation * Time.deltaTime;
		//Die orientation Werte müssen auf (0 - 360) limitiert werden
		if (orientation < 0.0f) {
			orientation += 360.0f;
		} else if (orientation > 360.0f) {
			orientation -= 360.0f;
		}

		transform.Translate (displacement, Space.World);
		transform.rotation = new Quaternion ();
		transform.Rotate (Vector3.up, orientation);
	}

	public virtual void LateUpdate ()
	{
		velocity += controlling.linear * Time.deltaTime;
		rotation += controlling.angular * Time.deltaTime;

		if (velocity.magnitude > maxSpeed) {
			velocity.Normalize ();
			velocity = velocity * maxSpeed;
		}

		if (controlling.angular == 0.0f) {
			rotation = 0.0f;
		}
		if (controlling.linear.sqrMagnitude == 0.0f) {
			velocity = Vector3.zero;
		}
		controlling = new Controlling ();

	}
		
	public void SetControlling (Controlling controlling)
	{
		this.controlling = controlling;
	}
}

