﻿using UnityEngine;
using System.Collections;

public class Seek : AIBehaviour
{
	public override Controlling GetControlling ()
	{
		Controlling controlling = new Controlling ();
		controlling.linear = target.transform.position - transform.position;
		controlling.linear.Normalize ();
		controlling.linear = controlling.linear * aiPlayer.maxAccel;
		return controlling;
	}
}

