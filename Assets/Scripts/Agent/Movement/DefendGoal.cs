﻿using UnityEngine;
using System.Collections;

public class DefendGoal : AIBehaviour
{
	public float targetRadius;
	public float slowRadius;
	public float timeToTarget;

	public override void Start ()
	{
		base.Start ();
		target = GameObject.FindWithTag ("ball");
		targetRadius = 0.2f;
		slowRadius = 0.4f;
		timeToTarget = 0.1f;
	}

	public override Controlling GetControlling ()
	{
		Controlling controlling = new Controlling ();
		//Vector3 direction = target.transform.position - transform.position;
		Vector3 direction = CalculateDefendPosition();
		float distance = direction.magnitude;
		float targetSpeed;
		if (distance < targetRadius) {
			return controlling;
		}
		if (distance > slowRadius) {
			targetSpeed = aiPlayer.maxSpeed;
		} else {
			targetSpeed = aiPlayer.maxSpeed * distance / slowRadius;
		}

		Vector3 desiredVelocity = direction;
		desiredVelocity.Normalize ();
		desiredVelocity *= targetSpeed;
		controlling.linear = desiredVelocity - aiPlayer.velocity;
		controlling.linear /= timeToTarget;
		if (controlling.linear.magnitude > aiPlayer.maxAccel) {
			controlling.linear.Normalize ();
			controlling.linear *= aiPlayer.maxAccel;
		}
		return controlling;
	}

	private Vector3 CalculateDefendPosition()
	{
		float defenseRadius = 15.0f;
		GameObject goal = aiPlayer.GetGoal ();
		Vector3 direction = target.transform.position - goal.transform.position;

		float intermediateResult = (direction.x * direction.x) + (direction.y * direction.y) + (direction.z * direction.z);
		Vector3 unitVector = direction / Mathf.Sqrt (intermediateResult);

		Vector3 distanceVector = unitVector * defenseRadius;
		Vector3 searchedVector = goal.transform.position + distanceVector;
		Vector3 searchedDistanceVec = searchedVector - transform.position;

		return searchedDistanceVec;
	}
		
}

