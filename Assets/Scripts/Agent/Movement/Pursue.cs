﻿using UnityEngine;
using System.Collections;

public class Pursue : Seek
{
	public float maxPrediction;
	private GameObject targetAux;
	private AITarget targetAgent;

	public override void Start ()
	{
		base.Start ();
		//print ("Finde'Ball");
		target = GameObject.FindWithTag ("ball");
		targetAgent = target.GetComponent<AITarget> ();
		maxPrediction = 20.0f;

		targetAux = target;
		target = new GameObject ();
	}
		
	public override Controlling GetControlling ()
	{
		Vector3 direction = targetAux.transform.position - transform.position;
		float distance = direction.magnitude;
		//float speed = agent.velocity.magnitude;
		float speed = aiPlayer.velocity.magnitude;
		float prediction;

		if (speed <= distance / maxPrediction) {
			prediction = maxPrediction;
		} else {
			prediction = distance / speed;
		}

		target.transform.position = targetAux.transform.position;
		target.transform.position += targetAgent.velocity * prediction;
		return base.GetControlling ();
	}
}

