﻿using UnityEngine;
using System.Collections;

public class AvoidWall : Seek
{
	public float avoidDistance;
	public float lookAhead;

	public override void Start ()
	{
		base.Start ();
		target = new GameObject ();
	}

	public override Controlling GetControlling ()
	{
		Controlling steering = new Controlling ();
		Vector3 position = transform.position;
		Vector3 rayVector = aiPlayer.velocity.normalized * lookAhead;
		Vector3 direction = rayVector;
		RaycastHit hit;

		if (Physics.Raycast (position, direction, out hit, lookAhead)) {
			position = hit.point + hit.normal * avoidDistance;
			aiPlayer.transform.position = position;
			steering = base.GetControlling ();
		}

		return steering;
	}
}

