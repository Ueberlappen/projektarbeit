﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AvoidPlayer : AIBehaviour
{
	public float collisionRadius = 1f;
	GameObject[] targets;

	// Use this for initialization
	public override void Start ()
	{
		targets = GameObject.FindGameObjectsWithTag ("AI");
	}
	
	public override Controlling GetControlling ()
	{
		Controlling steering = new Controlling ();
		float shortestTime = Mathf.Infinity;
		GameObject firstTarget = null;
		float firstMinSeperation = 0.0f;
		float firstDistance = 0.0f;
		Vector3 firstRelativePos = Vector3.zero;
		Vector3 firstRelativeVel = Vector3.zero;

		foreach (GameObject t in targets) {
			Vector3 relativePos;
			OfflineAI targetPlayer = t.GetComponent<OfflineAI> ();
			relativePos = t.transform.position - transform.position;
			Vector3 relativeVel = targetPlayer.velocity - aiPlayer.velocity;
			float relativeSpeed = relativeVel.magnitude;
			float timeToCollision = Vector3.Dot (relativePos, relativeVel);
			timeToCollision /= relativeSpeed * relativeSpeed * -1;
			float distance = relativePos.magnitude;
			float minSeparation = distance - relativeSpeed * timeToCollision;
			if (minSeparation > 2 * collisionRadius) {
				continue;
			}
			if (timeToCollision > 0.0f && timeToCollision < shortestTime) {
				shortestTime = timeToCollision;
				firstTarget = t;
				firstMinSeperation = minSeparation;
				firstRelativePos = relativePos;
				firstRelativeVel = relativeVel;
			}
		}

		if (firstTarget == null) {
			return steering;
		}
		if (firstMinSeperation <= 0.0f || firstDistance < 2 * collisionRadius) {
			firstRelativePos = firstTarget.transform.position;
		} else {
			firstRelativePos += firstRelativeVel * shortestTime;
		}
		firstRelativePos.Normalize ();
		steering.linear = -firstRelativePos * aiPlayer.maxAccel;
		return steering;
	}
}

