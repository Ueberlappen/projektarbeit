﻿using UnityEngine;
using System.Collections;

public class AIBehaviour : MonoBehaviour
{
	public GameObject target;
	protected AITarget aiTarget;
	protected OfflineAI aiPlayer;

	public virtual void Start()
	{
		//agent = gameObject.GetComponent<Agent> ();
		aiPlayer = gameObject.GetComponent<OfflineAI> ();
	}

	public virtual void FixedUpdate()
	{
		//agent.SetSteering (GetSteering ());
		aiPlayer.SetControlling (GetControlling ());
	}

	public virtual Controlling GetControlling()
	{
		return new Controlling ();
	}
}

